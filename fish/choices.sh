#!/usr/bin/fish

echo 'do you say yes or no?'
while true
    set yn (read -P "[y/n]: " ) 

	switch "$yn"
    case Y y yes
    	break
    case N n no
    	 echo "goodbye!" 
    	 exit
    case '*'
    	echo "Please answer yes or no."
    end
end

set var (read -P "insert variable: " )
echo "variable is $var" 

exit
