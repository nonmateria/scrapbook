#!/usr/bin/fish

set multiargs ""

for var in $argv
    set filepath (realpath "$var")
    set -a multiargs "$filepath "
end

echo "$multiargs"

exit
