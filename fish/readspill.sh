#!/usr/bin/fish

# enabling this line fixes the problem 
#set fish_history ""

set invisible1 (read -s -P "enter something secret: " )
set invisible2 (read -s -P "enter another secret: " )

echo 'now press up (press q to quit):'
while true
    set k (read -l -n 1 -P ">" ) 
	echo "keypress: $k"
	if test "$k" = "q"
		exit
	end		
end

exit
