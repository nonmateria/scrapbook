#!/usr/bin/fish
# will restart when crash
while true 
	folderkit -p /home/nicola/resources/testsamp/

	if test $status -eq 0 
		break
	end 
	echo "program quitted with an error, restarting..."
end

exit

#!/bin/sh
# will reboot the system on crash ( bash on  rpi)
(
    pixelscript /home/nicola/np-scrapbook/sketches/smoke_mantra/
    ret=$?
    if [ $ret -ne 0 ]; then
        sudo reboot
    fi
) &

exit