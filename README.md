# scrapbook

scrapbook of many snippets of code: bash scripts, c stubs and creative coding sketches.

For GLSL fragment shaders and lua scripts, you need [nsketch](https://codeberg.org/nonmateria/nsketch) or [scriptsuite](https://codeberg.org/nonmateria/scriptsuite) to run them.

The code in this repository will change as the water flows, don't be attached to it =) 
