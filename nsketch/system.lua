
require "strict"

----------------------------------------------------
window.title( "testing system calls" )
window.size( 920, 300 )
layer.create( "def")

-- execyting bash command from pipe
local handle = io.popen( "figlet 'hello world' " )
local result = handle:read("*a")
--print ( result ) 
handle:close()

gfx.font("/usr/share/fonts/truetype/TerminusTTF-4.46.0.ttf", 32 )

----------------------------------------------------
function loop()
	layer.clear(0,0,0,255)
	gfx.text( result, 20, 20 ) 
end
