
require "strict"

window.size( 200, 200 )
layer.create( "def" )
layer.clear( 20, 0, 0, 255 )

----------------------------------------------------

osc.receiver( "4444" )

----------------------------------------------------
function loop()

	while osc.next() do
		print( "address : "..osc.address() )

		for i=0,osc.size()-1 do
			if osc.is_string(i) then
				print("    "..i.." str: "..osc.get_string(i))
			end
			if osc.is_number(i) then
				print("    "..i.." num: "..osc.get_number(i))
			end
		end
	end 
end

