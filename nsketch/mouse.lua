
require "strict"

-- thrustmaster dual analog 4 
da4 = { button_1=7, button_2=8, button_3=6, button_4=5,
        button_a=7, button_b=6, button_x=8, button_y=5,
		pad_up=1, pad_down=3, pad_left=4, pad_right=2,
		left_trigger=9, right_trigger=11, select=13, start=15, 
		left_axis_x=0, left_axis_y=1, right_axis_x=2, right_axis_y=3,
		left_axis_press=16, right_axis_press=17 }

----------------------------------------------------

local w = 480 
local h = 480

window.size( w, h )
layer.create( "def")
window.background( 0, 0, 0, 255 )

local cx = 0
local cy = 0

----------------------------------------------------
function loop()
	if mouse.down( mouse.right ) then 
		cx = mouse.x()
		cy = mouse.y()
	end 

	layer.clear(0,0,0,0)
	gfx.color( 255, 255, 255, 255 ) 
	gfx.line( mouse.x(), 0, mouse.x(), h ) 
	gfx.line( 0, mouse.y(), w, mouse.y() ) 
	gfx.circle( cx, cy, 12 )
end
