
require "strict"

----------------------------------------------------

local w = args.get_number("-w", 480) 
local h = args.get_number("-h", 480) 
local white = args.get_bool( "--white" )

window.size( w, h )
layer.create( "def")

----------------------------------------------------
function loop()
   	if white then
		layer.clear(255, 255, 255, 255)
   	else
 			layer.clear(0,0,0,0)
	end
end
