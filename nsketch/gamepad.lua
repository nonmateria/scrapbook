
require "strict"

-- thrustmaster dual analog 4 
da4 = { button_1=7, button_2=8, button_3=6, button_4=5,
        button_a=7, button_b=6, button_x=8, button_y=5,
		pad_up=1, pad_down=3, pad_left=4, pad_right=2,
		left_trigger=9, right_trigger=11, select=13, start=15, 
		left_axis_x=0, left_axis_y=1, right_axis_x=2, right_axis_y=3,
		left_axis_press=16, right_axis_press=17 }

----------------------------------------------------

window.size( 200, 200 )
layer.create( "def")
--window.background( 20, 20, 20, 255 )

local test = args.get_bool( "-t" )

----------------------------------------------------
function loop()
	layer.clear(0,0,0,0)

	if test then
		local last = pad.last_pressed() 
		if last ~= -1 then
			print( "last pressed is "..last )
		end
	end

	if pad.pressed( da4.button_1 ) then 
		print( "pressed button one" )
	end 

	local n = 1
	local a = pad.axis( 1 ) 
	if a ~= 0 then 
		print( "axis "..n.." = "..a )
    end
end
