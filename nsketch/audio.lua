
require "strict"

----------------------------------------------------
window.size( 80*2 + 60, 480 )
layer.create("def")

audio.band_to_name( 0, "cmus" )
	audio.attack( 5 ) 
	audio.release( 850 )
	--audio.highpass( 500 )
	audio.lowpass( 200 )

audio.band_to_id( 2, 2 )
	audio.attack( 2 ) 
	audio.release( 500 )
	audio.highpass( 200 )

----------------------------------------------------
function loop()
	layer.clear( 20, 20, 20, 255 )
	
	local env = fn.map( audio.get(0), 0.1, 0.7 ) 
	gfx.color( 255, 255, 255, 255 )
	gfx.rect( 20, 50, 80, (window.height()-100) * env ) 	

	env = fn.map( audio.get(2), 0.1, 0.6 ) 
	gfx.color( 255, 255, 255, 255 )
	gfx.rect( 120, 50, 80, (window.height()-100) * env ) 
end
