<CsoundSynthesizer>
<CsOptions>
	;-odac
	;-+rtaudio=jack -o dac
	-W -d -o ../../../../resources/folderkit/z/2/sample_zap_10ms.wav  
</CsOptions>

<CsInstruments>
	sr = 48000
	ksmps = 256
	nchnls = 1
	0dbfs  = 1
	
	gi_decay = 0.010
	gi_amt = 4.0
	gi_freq = 80 ; in hz
	
	gi_sine = ftgen( 0, 0, 1024*8, 10, 1 ) ;A SINE WAVE
	
	instr 1
		schedule 2, 0, gi_decay
	endin

	instr 2
		a_aenv expsega 1.0, gi_decay*0.333, 1.0, gi_decay*0.666 , 0.001
		a_penv expsega 1.0, gi_decay, 0.001
		a_osc = oscili( a_aenv, gi_freq * octave(gi_amt*a_penv), gi_sine ) 
		out a_osc
	endin

</CsInstruments>

<CsScore>
	i1 0 0.045
	e
</CsScore>
</CsoundSynthesizer>