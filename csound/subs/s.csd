<CsoundSynthesizer>
<CsOptions>
	;-odac
	;-+rtaudio=jack -o dac
	-W -d -o ../../../../resources/folderkit/u/s/loop_sine.wav 
</CsOptions>

<CsInstruments>
	sr = 44100
	ksmps = 1
	nchnls = 1
	0dbfs  = 1

	#include "../globals.inc"

	gi_decay = 40.0 / gi_rootfreq ; num cycles / freq 
	
	gi_sine = ftgen( 0, 0, 1024*8, 10, 1) ;A SINE WAVE

	instr 1
	schedule 2, 0, gi_decay
	endin

	instr 2
		a_osc = oscili( 1.0, gi_rootfreq, gi_sine )
		out a_osc
	endin
</CsInstruments>

<CsScore>
	i1 0 0.001 
	e 
</CsScore>
</CsoundSynthesizer>