<CsoundSynthesizer>
<CsOptions>
	;-odac
	;-+rtaudio=jack -o dac
	-W -d -o ../../../../resources/folderkit/ICASE/DCASE/generated_sub.wav 
</CsOptions>

<CsInstruments>
	sr = 48000
	ksmps = 1
	nchnls = 1
	0dbfs  = 1

	#include "../globals.inc"

	gi_decay = 4.0  
	
	gi_sine = ftgen( 0, 0, 1024*8, 10, 1)   ;A SINE WAVE

	instr 1
		schedule 2, 0, gi_decay
	endin

	instr 2		
		k_aenv = expseg( AMOUNT, DECAY, 1.0, gi_decay, 0.001 )
		a_osc = foscili( k_aenv, gi_rootfreq, 1.0, 1.0, 0.0, gi_sine )
		a_wf MODE a_osc, CONTROL, 0.0
		out a_wf
	endin
</CsInstruments>

<CsScore>
	i1 0 0.001 
	e 
</CsScore>
</CsoundSynthesizer>