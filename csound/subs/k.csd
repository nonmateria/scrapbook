<CsoundSynthesizer>
<CsOptions>
	;-odac
	;-+rtaudio=jack -o dac
	-W -d -o ../../../../resources/folderkit/u/k/subkick.wav  
</CsOptions>

<CsInstruments>
	sr = 44100
	ksmps = 1
	nchnls = 1
	0dbfs  = 1

	#include "../globals.inc"

	gi_decay = 2.5  
	
	gi_sine = ftgen( 0, 0, 1024*8, 10, 1)   ;A SINE WAVE

	instr 1
		schedule 2, 0, gi_decay
	endin

	instr 2		
		i_ratio = 1.25 
		k_menv = expseg( 4.0, 0.350, 0.001 )
		k_aenv = expseg( 1.0, gi_decay*0.4, 1.0, gi_decay*0.6 , 0.001 )
		a_osc = foscili( k_aenv, gi_rootfreq * octave(k_menv), 1.0, i_ratio, k_menv, gi_sine )
		out a_osc
	endin

</CsInstruments>

<CsScore>
	i1 0 0.001 
	e 
</CsScore>
</CsoundSynthesizer>