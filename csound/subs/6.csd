<CsoundSynthesizer>
<CsOptions>
	;-odac
	;-+rtaudio=jack -o dac
	-W -d -o ../../../../resources/folderkit/u/6/fm_sub_6.wav  
</CsOptions>

<CsInstruments>
	sr = 44100
	ksmps = 1
	nchnls = 1
	0dbfs  = 1

	#include "../globals.inc"

	gi_decay = 8.0  
	
	gi_sine = ftgen( 0, 0, 1024*8, 10, 1)   ;A SINE WAVE

	instr 1
		schedule 2, 0, gi_decay
	endin

	instr 2		
		k_menv = expseg( 5.0, 0.850, 0.001 )
		k_menv2 = linseg( 3.0, 0.450, 0.001 )
		k_aenv = expseg( 1.0, gi_decay*0.8, 1.0, gi_decay*0.2 , 0.001 )

		a1, a2 	crossfmi 1.0, 3.125, k_menv2, k_menv, gi_rootfreq, gi_sine, gi_sine 
		out a1 * k_aenv
	endin
</CsInstruments>

<CsScore>
	i1 0 0.001 
	e 
</CsScore>
</CsoundSynthesizer>