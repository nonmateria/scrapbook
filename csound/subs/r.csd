<CsoundSynthesizer>
<CsOptions>
	-odac
	;-+rtaudio=jack -o dac
	;-W -d -o ../../../../resources/folderkit/u/r/loop_pwm_reese.wav  
</CsOptions>

<CsInstruments>
	sr = 44100
	ksmps = 1
	nchnls = 1
	0dbfs  = 1

	#include "../globals.inc"

	gi_lfofreq = gi_rootfreq / 40.0 	
	gi_decay = 6.0 / gi_lfofreq ; num cycles / freq 	

	instr 1
	schedule 2, 0, gi_decay
	endin

	instr 2		
		k_lfo = lfo( 0.4, gi_lfofreq, 1 ) + 0.5
		a_osc = vco2( 1.0, gi_rootfreq, 2, k_lfo )
		a_lp = moogladder(  a_osc, 150.0, 0.0 )
		out	a_lp 
	endin

</CsInstruments>

<CsScore>
	i1 0 0.001 
	e 
</CsScore>
</CsoundSynthesizer>