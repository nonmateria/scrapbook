<CsoundSynthesizer>
<CsOptions>
	-odac
	-W -d -o reese_F1.wav  
</CsOptions>

<CsInstruments>
	sr = 44100
	ksmps = 1
	nchnls = 1
	0dbfs  = 1

	gi_freq = cpspch(5.05) ; sub F
	;gi_freq = cpspch(6.05) ; upper octave to sub F

	gi_decay init 8.0 ; 8 sec 

	instr 1
	schedule 2, 0, gi_decay
	endin

	instr 2
		i_wave = 10 ; saw 
		k_aenv = expseg( 1.0, gi_decay*0.75, 1.0, gi_decay*0.25 , 0.001 )
		a_osc1 = vco2( k_aenv, gi_freq * octave( 0.3 /12.0), 10, 0.01 ) * 0.5
		a_osc2 = vco2( k_aenv, gi_freq * octave(-0.3 /12.0), 4, 0.01 ) * 0.5
		;alp, ahigh, aband   svfilter  a_osc1 + a_osc2, 120.0, 0.0 
		;alp  moogladder  a_osc1 + a_osc2, 300.0, 0.0 
		;alp lpf18  a_osc1 + a_osc2, 200.0, 0.0, 2.0 
		alp = a_osc1 + a_osc2
		out alp
	endin
</CsInstruments>

<CsScore>
	i1 0 2.0 
	e 
</CsScore>
</CsoundSynthesizer>