<CsoundSynthesizer>
<CsOptions>
	;-odac
	;-+rtaudio=jack -o dac
	-W -d -o ../../../../resources/folderkit/ICASE/DCASE/generated_sub.wav  
</CsOptions>

<CsInstruments>
	sr = 48000
	ksmps = 1
	nchnls = 1
	0dbfs  = 1

	#include "../globals.inc"

	gi_decay = 4.0  
	
	gi_sine = ftgen( 0, 0, 1024*8, 10, 1)   ;A SINE WAVE

	instr 1
		schedule 2, 0, gi_decay
	endin

	instr 2		
		k_menv = linseg( 1.0, DECAY, 0.001)
		k_menv2 = expseg( AMOUNT, DECAY, 0.001 )
		k_aenv = expseg( 1.0, DECAY, 1.0, gi_decay, 0.001 )

		a1, a2 	crossfmi 1.0, RATIO, k_menv, k_menv2, gi_rootfreq, gi_sine, gi_sine 
		out a1 * k_aenv
	endin
</CsInstruments>

<CsScore>
	i1 0 0.001 
	e 
</CsScore>
</CsoundSynthesizer>