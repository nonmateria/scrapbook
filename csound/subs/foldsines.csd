<CsoundSynthesizer>
<CsOptions>
	;-odac
	;-+rtaudio=jack -o dac
	-W -d -o sub_to_fold.wav  
</CsOptions>

<CsInstruments>
	sr = 48000
	ksmps = 1
	nchnls = 1
	0dbfs  = 1

	gi_rootfreq = cpspch(5.05)  
	print gi_rootfreq	
	
	gi_decay = 8.0  
	
	gi_sine = ftgen( 0, 0, 1024*8, 10, 1)   ;A SINE WAVE

	instr 1
		schedule 2, 0, gi_decay
	endin

	instr 2		
		k_aenv = expseg( 1.0, gi_decay*0.8, 1.0, gi_decay*0.2 , 0.001 )
		a_osc1 = oscili( 1.0, gi_rootfreq, gi_sine ) * k_aenv
		a_osc2 = oscili( 1.0, gi_rootfreq*2.0, gi_sine ) * k_aenv
		out a_osc1*0.5 + a_osc2*0.5
	endin

</CsInstruments>

<CsScore>
	i1 0 0.001 
	e 
</CsScore>
</CsoundSynthesizer>