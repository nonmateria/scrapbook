#!/usr/bin/fish 

#set orcanums 1 2 3 4 5 6 7 8 9 a b c d e f g 
set orcanums 1 2 3 4 5 6 7 8 9 a b c d

csound base_sine.csd

function generate_ratio
    set ICASE $argv[1]
    set RATIO $argv[2]
    set AMOUNT $argv[3]

    for i in ( seq 13 )
        set dcase $orcanums[$i]
        set DECAY (math "( ($i*$i)/256.0 ) * 1.800 ")

        echo "$dcase | $DECAY | $RATIO | $AMOUNT"

        cp base.csd edited.csd

        sed -i -e "s|DECAY|$DECAY|g" edited.csd
        sed -i -e "s|DCASE|$dcase|g" edited.csd
        sed -i -e "s|ICASE|$ICASE|g" edited.csd
        sed -i -e "s|RATIO|$RATIO|g" edited.csd
        sed -i -e "s|AMOUNT|$AMOUNT|g" edited.csd

        mkdir -p ~/resources/folderkit/$ICASE/$dcase

        csound edited.csd
    end

    mkdir -p ~/resources/folderkit/$ICASE/0
    cp sine_sub.wav ~/resources/folderkit/$ICASE/0/generated_sub.wav
end

function generate_ratio_2
    set ICASE $argv[1]
    set RATIO $argv[2]
    set AMOUNT $argv[3]

    for i in ( seq 13 )
        set dcase $orcanums[$i]
        set DECAY (math "( ($i*$i)/256.0 ) * 1.800 ")

        echo "$dcase | $DECAY | $RATIO | $AMOUNT"

        cp base2.csd edited.csd

        sed -i -e "s|DECAY|$DECAY|g" edited.csd
        sed -i -e "s|DCASE|$dcase|g" edited.csd
        sed -i -e "s|ICASE|$ICASE|g" edited.csd
        sed -i -e "s|RATIO|$RATIO|g" edited.csd
        sed -i -e "s|AMOUNT|$AMOUNT|g" edited.csd

        mkdir -p ~/resources/folderkit/$ICASE/$dcase

        csound edited.csd
    end

    mkdir -p ~/resources/folderkit/$ICASE/0
    cp sine_sub.wav ~/resources/folderkit/$ICASE/0/generated_sub.wav
end


function generate_drop
    set ICASE $argv[1]
    set AMOUNT $argv[2]

    for i in ( seq 13 )
        set dcase $orcanums[$i]
        set DECAY (math "( ($i*$i)/256.0 ) * 1.800 ")

        echo "$dcase | $DECAY"

        cp basedrop.csd edited.csd

        sed -i -e "s|DECAY|$DECAY|g" edited.csd
        sed -i -e "s|DCASE|$dcase|g" edited.csd
        sed -i -e "s|ICASE|$ICASE|g" edited.csd
        sed -i -e "s|AMOUNT|$AMOUNT|g" edited.csd

        mkdir -p ~/resources/folderkit/$ICASE/$dcase

        csound edited.csd
    end

    mkdir -p ~/resources/folderkit/$ICASE/0
    cp sine_sub.wav ~/resources/folderkit/$ICASE/0/generated_sub.wav
end

function generate_folded
    set ICASE $argv[1]
    set MODE $argv[2]
    set AMOUNT $argv[3]
    set CONTROL $argv[4]

    for i in ( seq 13 )
        set dcase $orcanums[$i]
        set DECAY (math "( ($i*$i)/256.0 ) * 1.800 ")

        echo "$dcase | $DECAY | $MODE | $AMOUNT | $CONTROL"

        cp basefold.csd edited.csd

        sed -i -e "s|DECAY|$DECAY|g" edited.csd
        sed -i -e "s|DCASE|$dcase|g" edited.csd
        sed -i -e "s|ICASE|$ICASE|g" edited.csd
        sed -i -e "s|MODE|$MODE|g" edited.csd
        sed -i -e "s|AMOUNT|$AMOUNT|g" edited.csd
        sed -i -e "s|CONTROL|$CONTROL|g" edited.csd

        mkdir -p ~/resources/folderkit/$ICASE/$dcase

        csound edited.csd
    end

    mkdir -p ~/resources/folderkit/$ICASE/0
    cp sine_sub.wav ~/resources/folderkit/$ICASE/0/generated_sub.wav
end

#generate_drop 1 3.0
#generate_drop 2 4.0
#generate_drop 3 5.0
#generate_drop 4 6.0

generate_ratio 1 2.0 3.0
generate_ratio 2 1.5 8.0
generate_ratio_2 3 1.0 9.0
generate_ratio_2 4 2.0 8.0
generate_ratio 5 3.0 4.0
generate_ratio 6 3.0 6.0
generate_ratio 7 4.0 2.0
generate_ratio 8 4.0 4.0
generate_ratio 9 3.5 6.0

generate_folded a sinefold 6.0 0.2
generate_folded b sinefold 4.0 0.5
generate_folded c trifold  6.0 0.5

exit
