#!/bin/bash 

for x in $(seq 1 53); do
	csound st1.csd 
	mv wave_noise_64.wav "$HOME/resources/folderkit/m/1/wave_noise_64_$x.wav"
	
	csound st2.csd 
	mv wave_noise_128.wav "$HOME/resources/folderkit/m/2/wave_noise_128_$x.wav"

	csound st3.csd 
	mv wave_noise_256.wav "$HOME/resources/folderkit/m/3/wave_noise_256_$x.wav"
	
	csound st4.csd 
	mv wave_noise_512.wav "$HOME/resources/folderkit/m/4/wave_noise_512_$x.wav"
	
	csound st5.csd 
	mv wave_noise_1024.wav "$HOME/resources/folderkit/m/5/wave_noise_1024_$x.wav"
done

exit