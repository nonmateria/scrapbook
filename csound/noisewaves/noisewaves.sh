#!/bin/bash 

for x in $(seq 1 53); do
	csound 1.csd 
	mv wave_noise_64.wav "$HOME/resources/folderkit/n/1/wave_noise_64_$x.wav"
	
	csound 2.csd 
	mv wave_noise_128.wav "$HOME/resources/folderkit/n/2/wave_noise_128_$x.wav"

	csound 3.csd 
	mv wave_noise_256.wav "$HOME/resources/folderkit/n/3/wave_noise_256_$x.wav"
	
	csound 4.csd 
	mv wave_noise_512.wav "$HOME/resources/folderkit/n/4/wave_noise_512_$x.wav"
	
	csound 5.csd 
	mv wave_noise_1024.wav "$HOME/resources/folderkit/n/5/wave_noise_1024_$x.wav"
done

exit