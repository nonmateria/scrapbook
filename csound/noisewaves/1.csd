<CsoundSynthesizer>
<CsOptions>
	;-odac
	;-+rtaudio=jack -o dac
	-W -d -o wave_noise_64.wav    
</CsOptions>

<CsInstruments>
	sr = 44100
	ksmps = 1
	nchnls = 1
	0dbfs  = 1

	gi_freq = 44100 / 64
	gi_decay init 64 / 44100

	seed 0
	
	; 128 for 7 and 8, 256 for 6, 512 for the other octaves 
	gi_noise ftgen 0, 0, 64, 21, 1   ; positive noise table

	instr 1
		schedule 2, 0, gi_decay
	endin

	instr 2
		a_osc = oscili( 1.0, gi_freq , gi_noise ) * 2.0 - 1.0
		out a_osc
	endin
</CsInstruments>

<CsScore>
	i1 0 [64 / 44100 ]
	e
</CsScore>
</CsoundSynthesizer>