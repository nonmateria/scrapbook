<CsoundSynthesizer>
<CsOptions>
	;-odac
	;-+rtaudio=jack -o dac
	-W -d -o ../../../../resources/folderkit/l/x/sample_exp_click_10ms_pos.wav
</CsOptions>

<CsInstruments>
	sr = 48000
	ksmps = 256
	nchnls = 1
	0dbfs  = 1

	gi_decay init 0.010

	instr 1
		schedule 2, 0, gi_decay
	endin

	instr 2
		a_seg expsega 1.0, gi_decay, 0.001
		;a_seg linseg 1.0, gi_decay, 0.0
		out a_seg
	endin

</CsInstruments>

<CsScore>
	i1 0 0.010
	e
</CsScore>
</CsoundSynthesizer>