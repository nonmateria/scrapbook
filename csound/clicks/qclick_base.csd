<CsoundSynthesizer>
<CsOptions>
	;-odac
	-W -d -o click.wav   
</CsOptions>

<CsInstruments>
	sr = 44100
	ksmps = 256
	nchnls = 1
	0dbfs  = 1

	#include "../globals.inc"

	seed 0
	
	instr 1
		schedule 2, 0, gi_clickdecay
	endin

	instr 2
		aseg expsega 1.0, gi_clickdecay, 0.001
		anoise = fractalnoise( 1.0, NOISE )
		out anoise * aseg * ampdb( AMP )
	endin
</CsInstruments>

<CsScore>
	i1 0 0.030
	e
</CsScore>
</CsoundSynthesizer>