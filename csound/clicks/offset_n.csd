<CsoundSynthesizer>
<CsOptions>
	;-odac
	;-+rtaudio=jack -o dac
	-W -d -o wave_negative_offset.wav
</CsOptions>

<CsInstruments>
	sr = 44100
	ksmps = 1
	nchnls = 1
	0dbfs  = 1

	#include "../globals.inc"

	gi_freq = 44100 / (1024)
	
	gi_decay init 1024 / 44100

	instr 1
		schedule 2, 0, gi_decay
	endin

	instr 2
		a_osc = -1.0
		out a_osc
	endin

</CsInstruments>

<CsScore>
	i1 0 [1024 / 44100]
	e 
</CsScore>
</CsoundSynthesizer>