<CsoundSynthesizer>
<CsOptions>
	;-odac
	-W -d -o click.wav   
</CsOptions>

<CsInstruments>
	sr = 48000
	ksmps = 256
	nchnls = 1
	0dbfs  = 1

	#include "../globals.inc"

	seed 0
	
	instr 1
		schedule 2, 0, gi_exciterdecay
	endin

	instr 2
		aseg expsega 1.0, gi_exciterdecay, 0.001
		;a_seg linseg 1.0, gi_decay, 0.0
		anoise = noise( 1.0, 0.0 )
		afreq = gi_rootfreq  * octave( BASEOCT + MODOCT * aseg ) ; 12 / 8 / 4 / 0 
		alp  moogladder anoise, afreq , 0.0 
		out alp * aseg * ampdb( AMP )
	endin
</CsInstruments>

<CsScore>
	i1 0 0.020
	e
</CsScore>
</CsoundSynthesizer>