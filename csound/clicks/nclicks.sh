#!/usr/bin/fish 

function generate_click
    set CASE $argv[1]
    set OCT $argv[2]
    set MOD $argv[3]
    set NOISE $argv[4]
    set AMP $argv[5]

	for x in (seq 53)
		cp nclick_base.csd edited.csd
	
        sed -i -e "s|BASEOCT|$OCT|g" edited.csd
        sed -i -e "s|MODOCT|$MOD|g" edited.csd
        sed -i -e "s|NOISE|$NOISE|g" edited.csd
        sed -i -e "s|AMP|$AMP|g" edited.csd

		csound edited.csd 
		mkdir -p "$HOME/resources/folderkit/l/$CASE/"
		cp click.wav "$HOME/resources/folderkit/l/$CASE/sample_nclick_$x.wav"
	end
end

function generate_exciter
    set FOLDER $argv[1]
    set ICASE $argv[2]
    set OCT $argv[3]
    set MOD $argv[4]
    set AMP $argv[5]

	for x in (seq 53)
		cp kclick_base.csd edited.csd
	
        sed -i -e "s|BASEOCT|$OCT|g" edited.csd
        sed -i -e "s|MODOCT|$MOD|g" edited.csd
        sed -i -e "s|AMP|$AMP|g" edited.csd

		csound edited.csd 
		mkdir -p "$HOME/resources/folderkit/$FOLDER/$ICASE/"
		cp click.wav "$HOME/resources/folderkit/$FOLDER/$ICASE/sample_nclick_$x.wav"
	end
end

function generate_pink_decay
    set FOLDER $argv[1]
    set ICASE $argv[2]
    set DECAY $argv[3]
    set AMP $argv[5]

	for x in (seq 53)
		cp pclick_base.csd edited.csd
	
        sed -i -e "s|BASEOCT|$OCT|g" edited.csd
        sed -i -e "s|MODOCT|$MOD|g" edited.csd
        sed -i -e "s|DECAY|$DECAY|g" edited.csd
        sed -i -e "s|AMP|$AMP|g" edited.csd

		csound edited.csd 
		mkdir -p "$HOME/resources/folderkit/$FOLDER/$ICASE/"
		cp click.wav "$HOME/resources/folderkit/$FOLDER/$ICASE/sample_nclick_$x.wav"
	end
end

function generate_all_clicks
	#             icase oct 	mod		noise 	amp
	generate_click 1 	3.0 	1.0 	1.0		3.0
	generate_click 2 	3.0 	1.5 	1.0		3.0
	generate_click 3 	3.0 	2.0 	1.0		3.0
	generate_click 4 	3.0 	2.5 	1.0		3.0
	generate_click 5 	3.0 	3.0 	1.0		3.0
	generate_click 6 	3.0 	3.5 	1.0		3.0
	generate_click 7 	3.0 	4.0 	1.0		3.0
	generate_click 8 	3.0 	4.5 	1.0		2.0
	generate_click 9 	3.0 	5.0 	1.0		1.0
	generate_click a 	3.0 	5.5 	1.0		-1.0
	generate_click b 	3.0 	6.0 	1.0		-3.0
	generate_click c 	3.0 	6.5 	1.0		-3.0
	generate_click d 	3.0 	7.0 	1.0		-3.0
end


function generate_all_karplus

	generate_exciter 	k	1 	2.0 	1.0 	-4.0
	generate_exciter 	k	2 	3.0 	1.0 	-4.0
	generate_exciter 	k	3 	3.0 	1.5 	-4.0
	generate_exciter 	k	4 	3.0 	2.0 	-4.0
	generate_exciter 	k	5 	3.0 	2.25 	-4.0
	generate_exciter 	k 	6 	3.0 	2.5 	-4.0
	generate_exciter 	k 	7 	3.0 	2.75 	-4.0
	generate_exciter 	k 	8 	3.0 	3.0 	-4.0
	generate_exciter 	k 	9 	3.0 	3.5 	-4.0
	generate_exciter 	k 	a 	3.0 	4.0 	-5.0
	generate_exciter 	k 	b 	3.0 	5.0 	-8.0
	generate_exciter 	k 	c 	3.0 	6.0 	-10.0
	generate_exciter 	k 	d 	3.0 	7.0 	-12.0

    # memo: old values
    # 3 ---> 5
    # 4 ---> 7
end

function generate_all_q

	generate_exciter 	q	1 	3.0		0.0	 	-10.0
	generate_exciter 	q	2 	3.5		0.0	 	-9.0
	generate_exciter 	q	3 	4.0		0.0	 	-8.0
	generate_exciter 	q	4 	4.5		0.0	 	-7.0
	generate_exciter 	q	5 	5.0 	0.0 	-6.0
	generate_exciter 	q	6 	5.5 	0.0 	-5.0
	generate_exciter 	q	7 	6.0 	0.0 	-4.0
	generate_exciter 	q	8 	6.5 	0.0 	-3.0
	generate_exciter 	q	9 	7.0 	0.0 	-2.0
	generate_exciter 	q	a 	7.5 	0.0 	-1.0
	generate_exciter 	q	b 	8.0 	0.0 	0.0
	generate_exciter 	q	c 	8.5 	0.0 	0.5
	generate_exciter 	q	d 	9.0 	0.0 	1.0

    # memo: old values
	# 2 -> 5
	# 3 --> 7
	# 4 --> 9
end

function generate_k_pink
	generate_pink_decay 	k	p 	0.005	-36
	generate_pink_decay 	k	q 	0.010	-24
	generate_pink_decay 	k	r 	0.020	-12	
	generate_pink_decay 	k	s 	0.040	-12	
end

#generate_all_clicks
#generate_all_karplus
generate_all_q
#generate_k_pink


exit
