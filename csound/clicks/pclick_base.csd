<CsoundSynthesizer>
<CsOptions>
	;-odac
	-W -d -o click.wav   
</CsOptions>

<CsInstruments>
	sr = 44100
	ksmps = 256
	nchnls = 1
	0dbfs  = 1
	
	seed 0
	
	instr 1
		schedule 2, 0, DECAY
	endin

	instr 2
		aseg expsega 1.0, DECAY, 0.001
		anoise = fractalnoise( 1.0, 1.0 )
		out anoise * aseg * ampdb( AMP )
	endin
</CsInstruments>

<CsScore>
	i1 0 DECAY
	e
</CsScore>
</CsoundSynthesizer>