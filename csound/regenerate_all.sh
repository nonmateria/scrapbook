#!/bin/bash

HERE=`pwd`

echo $HERE
if [ "$HERE" == "/home/$USER/lab/scrapbook/csound" ]; then
	cd clicks
	./nclicks.sh

	cd ../noiseloops
	./nloops.sh

	cd ../partials
	./gen_partials.sh

	cd ../subs 
	./subsgen.sh
	
	cd ../zaps
	csound 1.csd
	csound 2.csd
	csound 3.csd
	csound 4.csd
	csound 5.csd
	csound 6.csd
	csound 7.csd
	csound 8.csd
	csound 9.csd
	csound a.csd
	csound b.csd
	csound c.csd
	csound d.csd
	csound e.csd
	csound f.csd
	csound v.csd
	csound w.csd
	csound x.csd
	csound y.csd

	cd ../subs
	csound 1.csd
	csound 2.csd
	csound 3.csd
	csound 4.csd
	csound 5.csd
	csound 6.csd
	csound 7.csd
	csound k.csd
	csound r.csd
	csound s.csd

	cd ..
fi


exit