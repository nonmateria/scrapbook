<CsoundSynthesizer>
<CsOptions>
	;-odac
	;-+rtaudio=jack -o dac
	-W -d -o wave_sines_mix_r.wav
</CsOptions>

<CsInstruments>
	sr = 44100
	ksmps = 1
	nchnls = 1
	0dbfs  = 1

	#include "../globals.inc"

	gi_part1 = 1.0
	gi_part2 = 3.0
	gi_part3 = 5.0
	gi_part4 = 7.0
	gi_part5 = 9.0
	
	gi_freq1 = 44100 / (1024 / gi_part1 )
	gi_freq2 = 44100 / (1024 / gi_part2 )
	gi_freq3 = 44100 / (1024 / gi_part3 )
	gi_freq4 = 44100 / (1024 / gi_part4 )
	gi_freq5 = 44100 / (1024 / gi_part5 )
	gi_amp1 = 1.0 / (gi_part1*gi_part1)
	gi_amp2 = 1.0 / (gi_part2*gi_part2)
	gi_amp3 = 1.0 / (gi_part3*gi_part3)
	gi_amp4 = 1.0 / (gi_part4*gi_part4)
	gi_amp5 = 1.0 / (gi_part5*gi_part5)
	gi_ampdiv = 1.0 / (gi_amp1 + gi_amp2 + gi_amp3 + gi_amp4 + gi_amp5) 
	
	gi_decay init 1024 / 44100
	
	gi_sine = ftgen( 0, 0, 1024*32.0, 10, 1 )  ;A SINE WAVE

	instr 1
		schedule 2, 0, gi_decay
	endin

	instr 2
		a_osc1 = oscili( 1.0, gi_freq1, gi_sine )
		a_osc2 = oscili( 1.0, gi_freq2, gi_sine )
		a_osc3 = oscili( 1.0, gi_freq3, gi_sine )
		a_osc4 = oscili( 1.0, gi_freq4, gi_sine )
		a_osc5 = oscili( 1.0, gi_freq5, gi_sine )
		a_out = a_osc1 * gi_amp1 + a_osc2 * gi_amp2 + a_osc3 * gi_amp3 + a_osc4 * gi_amp4 + a_osc5 * gi_amp5
		out a_out * gi_ampdiv
	endin

</CsInstruments>

<CsScore>
	i1 0 [1024 / 44100]
	e 
</CsScore>
</CsoundSynthesizer>