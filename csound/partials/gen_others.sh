#!/bin/bash 

csound p123.csd
csound p124.csd
csound p134.csd
csound p234.csd
csound p1234.csd
csound p1_half2.csd 

mv wave_sines_r123.wav ~/resources/folderkit/s/g/
mv wave_sines_r124.wav ~/resources/folderkit/s/h/
mv wave_sines_r134.wav ~/resources/folderkit/s/i/
mv wave_sines_r234.wav ~/resources/folderkit/s/j/
mv wave_sines_r1234.wav ~/resources/folderkit/s/k/
mv wave_sines_r1_half2.wav ~/resources/folderkit/s/v/

exit
