<CsoundSynthesizer>
<CsOptions>
	;-odac
	;-+rtaudio=jack -o dac
	-W -d -o wave_sine_r9.wav
</CsOptions>

<CsInstruments>
	sr = 44100
	ksmps = 1
	nchnls = 1
	0dbfs  = 1

	#include "../globals.inc"

	gi_freq = 44100 / (1024 / 9.0 )
	
	gi_decay init 1024 / 44100
	
	gi_sine = ftgen( 0, 0, 1024*32.0, 10, 1 )  ;A SINE WAVE

	instr 1
		schedule 2, 0, gi_decay
	endin

	instr 2
		a_osc = oscili( 1.0, gi_freq, gi_sine )
		out a_osc
	endin

</CsInstruments>

<CsScore>
	i1 0 [1024 / 44100]
	e 
</CsScore>
</CsoundSynthesizer>