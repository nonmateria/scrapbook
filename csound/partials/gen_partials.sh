#!/bin/bash 

mkdir -p output

build_partial(){
    # generate navigation pages 
    partialname="$1"
    partialvalue="$2"
    
    echo "---------- BUILD PARTIAL $1 $2 ----------"

	cp base.csd "output/$partialname.csd"
	
    sed -i -e "s|PARTIALNAME|$partialname|g" "output/$partialname.csd"
    sed -i -e "s|PARTIALVALUE|$partialvalue|g" "output/$partialname.csd"

	mkdir -p ~/resources/folderkit/s/$partialname

	csound "output/$partialname.csd"

	mv "wave_sine_r$partialvalue.wav" "$HOME/resources/folderkit/s/$partialname/wave_sine_r$partialvalue.wav"
	
	rm ~/resources/folderkit/s/$partialname/*.dB
	touch ~/resources/folderkit/s/$partialname/-24.dB
}

build_partial 1 1
build_partial 2 2
build_partial 3 3
build_partial 4 4
build_partial 5 5
build_partial 6 6
build_partial 7 7
build_partial 8 8
build_partial 9 9 

exit
