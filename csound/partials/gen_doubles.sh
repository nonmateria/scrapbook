#!/bin/bash 

mkdir -p output

build_partial(){
    # generate navigation pages 
    wavename="$1"
    partial1="$2"
    partial2="$3"
    
    echo "---------- BUILD PARTIAL $1 $2 ----------"

	cp base2.csd "output/$wavename.csd"
	
    sed -i -e "s|PARTIALVALUE1|$partial1|g" "output/$wavename.csd"
    sed -i -e "s|PARTIALVALUE2|$partial2|g" "output/$wavename.csd"

	mkdir -p ~/resources/folderkit/s/$wavename

	csound "output/$wavename.csd"

	mv wave_sines*.wav "$HOME/resources/folderkit/s/$wavename/"
	
	rm ~/resources/folderkit/s/$wavename/*.dB
	touch ~/resources/folderkit/s/$wavename/-24.dB
}

# TODO those index are not correct anymore
build_partial a 1 2
build_partial b 1 3
build_partial c 1 4
build_partial d 2 3
build_partial e 2 4
build_partial f 3 4

exit
