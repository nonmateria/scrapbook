<CsoundSynthesizer>
<CsOptions>
	;-odac
	;-+rtaudio=jack -o dac
	-W -d -o foldwaves.wav
</CsOptions>

<CsInstruments>
	sr = 44100
	ksmps = 1
	nchnls = 1
	0dbfs  = 1

	#include "../globals.inc"

	gi_freq1 = 44100 / (1024 / 1.0 )
	gi_freq2 = 44100 / (1024 / 2.0 )
	
	gi_decay init 1024 / 44100
	
	gi_sine = ftgen( 0, 0, 1024*32.0, 10, 1 )  ;A SINE WAVE

	instr 1
		schedule 2, 0, gi_decay
	endin

	instr 2
		a_osc1 = oscili( 1.0, gi_freq1, gi_sine )
		a_osc2 = oscili( 1.0, gi_freq2, gi_sine )
		//a_out = a_osc1 * 0.666 + a_osc2 * 0.666 * (1.0 / PARTIALVALUE)
		a_out = a_osc1 * 0.5 + a_osc2 * 0.5 * (1.0/4.0)
		out a_out
	endin

</CsInstruments>

<CsScore>
	i1 0 [1024 / 44100]
	e 
</CsScore>
</CsoundSynthesizer>