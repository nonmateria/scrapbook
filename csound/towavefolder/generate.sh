#!/bin/bash 

build_partial(){
    # generate navigation pages 
    partialname="$1"

	mkdir -p ~/resources/folderkit/u/$partialname

	rm ~/resources/folderkit/u/$partialname/*.dB
	touch ~/resources/folderkit/u/$partialname/-15.dB

	csound $partialname.csd
	
	mv foldwaves.wav ~/resources/folderkit/u/$partialname/
}

build_partial 1
build_partial 2
build_partial 3
build_partial 4
build_partial 5

exit
