<CsoundSynthesizer>
<CsOptions>
	;-odac
	-W -d -o loop_pink_noise.wav   
</CsOptions>

<CsInstruments>
	sr = 44100
	ksmps = 256
	nchnls = 1
	0dbfs  = 1

	seed 0
	
	gi_decay init 0.5
	
	instr 1
		schedule 2, 0, gi_decay
	endin

	instr 2
		anoise = fractalnoise( 1.0, 1.0 )
		out anoise * ampdb(0.0)
	endin
</CsInstruments>

<CsScore>
	i1 0 0.5
	e
</CsScore>
</CsoundSynthesizer>