#!/bin/bash 

for x in $(seq 1 18); do
	csound white.csd 
	mv loop_white_noise.wav "$HOME/resources/folderkit/n/w/loop_white_noise_$x.wav"

	csound pink.csd 
	mv loop_pink_noise.wav "$HOME/resources/folderkit/n/p/loop_pink_noise_$x.wav"

	csound brown.csd 
	mv loop_brown_noise.wav "$HOME/resources/folderkit/n/b/loop_brown_noise_$x.wav"
done

exit