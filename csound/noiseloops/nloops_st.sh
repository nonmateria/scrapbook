#!/bin/bash 

for x in $(seq 1 18); do
	csound white.csd 
	mv loop_white_noise.wav white0.wav
	csound white.csd 
	mv loop_white_noise.wav white1.wav
	sox -M -c 1 white0.wav -c 1 white1.wav loop_white_noise.wav
	mv loop_white_noise.wav "$HOME/resources/folderkit/m/w/loop_white_noise_$x.wav"
	
	csound pink.csd 
	mv loop_pink_noise.wav pink0.wav
	csound pink.csd 
	mv loop_pink_noise.wav pink1.wav
	sox -M -c 1 pink0.wav -c 1 pink1.wav loop_pink_noise.wav
	mv loop_pink_noise.wav "$HOME/resources/folderkit/m/p/loop_pink_noise_$x.wav"

	csound brown.csd 
	mv loop_brown_noise.wav brown0.wav
	csound brown.csd 
	mv loop_brown_noise.wav brown1.wav
	sox -M -c 1 brown0.wav -c 1 brown1.wav loop_brown_noise.wav
	mv loop_brown_noise.wav "$HOME/resources/folderkit/m/b/loop_brown_noise_$x.wav"
done

exit