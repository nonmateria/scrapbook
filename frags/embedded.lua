
require "strict"

----------------------------------------------------
window.size( 480, 480 )
layer.create( "def")

frag.code ( [[
	#ifdef GL_ES
	precision mediump float;
	#endif

	uniform vec2 u_resolution;
	uniform float u_time;

	void main(){
		vec2 st = gl_FragCoord.xy/u_resolution;
		gl_FragColor = vec4( 1.0, cos(u_time*3.0)*0.3 + 0.3, 0.0 , 1.0 );
	}
]], "shader" ) 

----------------------------------------------------
function loop()
	layer.clear(0,0,0,255)
	frag.apply( "shader" )
end
