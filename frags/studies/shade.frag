
#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.14159265359
#define TWO_PI 6.28318530718

uniform vec2 u_resolution;
uniform float u_time;

// ------------------- SHADER ----------------------------------
void main(){
  
  vec2 st = gl_FragCoord.xy/u_resolution.xy;
  float s = smoothstep( 0.4, 0.6, st.x );
  gl_FragColor = vec4( vec3(s), 1.0 );
  
}
