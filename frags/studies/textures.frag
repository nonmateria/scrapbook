
#ifdef GL_ES
precision mediump float;
#endif

#ifndef PI
#define PI 3.1415926535897932384626433832795
#endif

#ifndef TWO_PI
#define TWO_PI 6.2831853071795864769252867665590
#endif

float stroke( float x, float d, float w, float fade ){ 
    return smoothstep(d-fade, d+fade, x+w*.5) - smoothstep(d-fade, d+fade, x-w*.5); 
} 
    
float stroke(float x, float d, float w){ 
    float r = step(d,x+w*.5) - step(d,x-w*.5); 
    return clamp(r, 0., 1.); 
}

mat2 rotate2d( in float _angle ){ 
    return mat2(cos(_angle),-sin(_angle), sin(_angle),cos(_angle)); 
}

vec2 rotated( vec2 _st, in float _angle ){ 
    _st -= 0.5; 
    _st *= rotate2d( _angle ); 
    _st += 0.5; 
    return _st;
}

float poly_sdf(vec2 st, int V) { 
    st = st*2.-1.;
    float a = atan(st.x,st.y)+PI; 
    float r = length(st); 
    float v = TWO_PI/float(V); 
    return cos(floor(.5+a/v)*v-a)*r; 
}

float map( float value, float minin, float maxin, float minout, float maxout ){ 
    value -= minin; 
    value = max( value, 0.0 ); 
    float range = maxin - minin; 
    value = min( value, range ); 
    float pct = value / range; 
    return mix( minout, maxout, pct );
}

//
// Description : GLSL 2D simplex noise function
//      Author : Ian McEwan, Ashima Arts
//  Maintainer : ijm
//     Lastmod : 20110822 (ijm)
//     License :
//  Copyright (C) 2011 Ashima Arts. All rights reserved.
//  Distributed under the MIT License. See LICENSE file.
//  https://github.com/ashima/webgl-noise
//

// Some useful functions
vec3 mod289(vec3 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
vec2 mod289(vec2 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
vec3 permute(vec3 x) { return mod289(((x*34.0)+1.0)*x); }

float noise(vec2 v) {

    // Precompute values for skewed triangular grid
    const vec4 C = vec4(0.211324865405187,
                        // (3.0-sqrt(3.0))/6.0
                        0.366025403784439,
                        // 0.5*(sqrt(3.0)-1.0)
                        -0.577350269189626,
                        // -1.0 + 2.0 * C.x
                        0.024390243902439);
                        // 1.0 / 41.0

    // First corner (x0)
    vec2 i  = floor(v + dot(v, C.yy));
    vec2 x0 = v - i + dot(i, C.xx);

    // Other two corners (x1, x2)
    vec2 i1 = vec2(0.0);
    i1 = (x0.x > x0.y)? vec2(1.0, 0.0):vec2(0.0, 1.0);
    vec2 x1 = x0.xy + C.xx - i1;
    vec2 x2 = x0.xy + C.zz;

    // Do some permutations to avoid
    // truncation effects in permutation
    i = mod289(i);
    vec3 p = permute(
            permute( i.y + vec3(0.0, i1.y, 1.0))
                + i.x + vec3(0.0, i1.x, 1.0 ));

    vec3 m = max(0.5 - vec3(
                        dot(x0,x0),
                        dot(x1,x1),
                        dot(x2,x2)
                        ), 0.0);

    m = m*m ;
    m = m*m ;

    // Gradients:
    //  41 pts uniformly over a line, mapped onto a diamond
    //  The ring size 17*17 = 289 is close to a multiple
    //      of 41 (41*7 = 287)

    vec3 x = 2.0 * fract(p * C.www) - 1.0;
    vec3 h = abs(x) - 0.5;
    vec3 ox = floor(x + 0.5);
    vec3 a0 = x - ox;

    // Normalise gradients implicitly by scaling m
    // Approximation of: m *= inversesqrt(a0*a0 + h*h);
    m *= 1.79284291400159 - 0.85373472095314 * (a0*a0+h*h);

    // Compute final noise value at P
    vec3 g = vec3(0.0);
    g.x  = a0.x  * x0.x  + h.x  * x0.y;
    g.yz = a0.yz * vec2(x1.x,x2.x) + h.yz * vec2(x1.y,x2.y);
    return 130.0 * dot(m, g);
}


// Simplex 3D noise
// 	<www.shadertoy.com/view/XsX3zB>
//	by Nikita Miropolskiy

/* discontinuous pseudorandom uniformly distributed in [-0.5, +0.5]^3 */
vec3 random3(vec3 c) {
	float j = 4096.0*sin(dot(c,vec3(17.0, 59.4, 15.0)));
	vec3 r;
	r.z = fract(512.0*j);
	j *= .125;
	r.x = fract(512.0*j);
	j *= .125;
	r.y = fract(512.0*j);
	return r-0.5;
}

const float F3 =  0.3333333;
const float G3 =  0.1666667;

float noise(vec3 p) {

	vec3 s = floor(p + dot(p, vec3(F3)));
	vec3 x = p - s + dot(s, vec3(G3));
	 
	vec3 e = step(vec3(0.0), x - x.yzx);
	vec3 i1 = e*(1.0 - e.zxy);
	vec3 i2 = 1.0 - e.zxy*(1.0 - e);
	 	
	vec3 x1 = x - i1 + G3;
	vec3 x2 = x - i2 + 2.0*G3;
	vec3 x3 = x - 1.0 + 3.0*G3;
	 
	vec4 w, d;
	 
	w.x = dot(x, x);
	w.y = dot(x1, x1);
	w.z = dot(x2, x2);
	w.w = dot(x3, x3);
	 
	w = max(0.6 - w, 0.0);
	 
	d.x = dot(random3(s), x);
	d.y = dot(random3(s + i1), x1);
	d.z = dot(random3(s + i2), x2);
	d.w = dot(random3(s + 1.0), x3);
	 
	w *= w;
	w *= w;
	d *= w;
	 
	return dot(d, vec4(52.0));
}

float noise_fractal(vec3 m) {
	return   0.5333333* noise(m)
				+0.2666667* noise(2.0*m)
				+0.1333333* noise(4.0*m)
				+0.0666667* 
                noise(8.0*m);
}


// if this random fails check out
// http://byteblacksmith.com/improvements-to-the-canonical-one-liner-glsl-rand-for-opengl-es-2-0/
float rand(vec2 st, float t){
    return fract(sin(dot(st.xy + fract(t*0.0013) ,vec2(12.9898,78.233))) * 43758.5453);
}
float rand( in float i, in float t ){
    return fract(sin(dot( vec2(i, t), vec2(12.9898,78.233))) * 43758.5453);
}

//-------------------------------------------------------------------------------

uniform float u_time;
uniform vec2 u_resolution;
uniform sampler2D u_tex0;

const float aa = 0.005;

float polymoire( vec2 st, int N, float spacing, float fatness, float speed ){
    vec2 rot = rotated( st, u_time*speed);
    float s0 = poly_sdf( rot, N );
    float t = stroke( fract( s0*spacing), .5, fatness, aa );
    return t;
}


float rangrid( vec2 st, float cols, float rows ){
    vec2 grid = floor( vec2(st.x * cols, st.y * rows) );
    float r = rand( grid, u_time );
    r = floor( r*4.99999 ) * 0.25;
    return r;
}


float expband( float coord, float num ) {
    float pos = coord - u_time*0.25;
    
    //float t = abs(fract( pos*num )*2.0 -1.0); // triangle
    float t = (sin( fract(pos*num)*TWO_PI ) + 1.0) * 0.5; // sine
    
    return t*t*t; // for slimmer bands multiply more u_times
}


float noisegrid( vec2 st ){
                        // num  * density
    float x = floor(st.x * 48.) * 8.0; 
    float y = floor(st.y * 48.) * 8.0;
    float r = noise( vec3( x, y, u_time*0.5 ) );
    
    r = map( r, 0.0, 0.6, 0.0, 1.0 );
    //r = floor( r*4.99999 ) * 0.25;
    return r;
}

float rectjit( vec2 st ){

    float xb = 2.0;
    float xb_jitter = 10.0;
    float yb = 24.0;
    float yb_jitter = 10.0;
    float density = 0.2;

    // mult grids    
    float t0 = rangrid( st, xb + rand(0., u_time)*xb_jitter, yb + rand(1., u_time) * yb_jitter );
    t0 = step( t0, density );
    float t1 = rangrid( st, xb + rand(2., u_time)*xb_jitter, yb + rand(3., u_time) * yb_jitter );
    t1 = step( t1, density);
    
    return t0 * t1;
}

void main (void) {
    vec2 st = gl_FragCoord.xy/u_resolution;
    float ratio = u_resolution.x / u_resolution.y;
	st.x *= ratio;
    
    // tiling
    //st *= 4.0;
    //st = fract(st);

    // warp coord-----------------density--------speed---amount
    float nx = st.x + noise(vec2(st.x*5.0, u_time*0.2)) * 0.3;
    //st.x = nx;

    float sx = st.x + rand(st.y, 0.0)*0.1;
    //st.x = sx;

    vec2 dt = rotated( st, PI * 0.05 );


    float t;

    // noise bands
    float t0 = noise( vec2( dt.y*4.0, u_time ) );
    t0 = step( 0.4, t0 );
    
    // lines 
    float t1 = rand( st.y*480.0*0.5, u_time );
    t1 = step( 0.95, t1 );
    
    float t2 = rangrid( st, 3., 6.);
    float t3 = polymoire( st+0.5, 8, 43.0, 0.25, 0.2 );
    float t4 = polymoire( st-0.5, 11, 43.0, 0.25, 0.2 );
    float t5 = expband( st.x, 6.0 );
    float t6 = noisegrid( st );
    
    // classic static
    float dust = rand( st, u_time ); 
    dust = map( dust, 0.9, 1.0, 0.0, 1. );
    //dust = step( 0.8 , dust );
        
    float t8 = fract( st.x * 21. );
    float t9 = fract( dt.x * (25.0 + sin(u_time)*5.0) );
    
    // scanlines y
    float t10 = step( 0.5, fract( st.y * 480.0*0.25) );
    
    float t11 = rectjit( dt );

    //t = t11*t0 + t3*t4*0.3;
    t = t11*t0 + dust;
    //t = t3 * t11;
    //t = t0 + t1 - 2.0*t0*t1;
    //t = t*t*t*t*t*t;
    t = min( t, 1.0 );
    
    //float s = stroke( circle_sdf(vec2(st.x-0.1, st.y)), 0.6, 0.2, aa );

    //float a = t + s - 2.0*t*s; // invert by shape s
    float a = t * 0.9;
	
    float undef;
    vec3 color = vec3( undef ) * a;
    vec3 white = vec3( 1.0 );

    gl_FragColor = vec4( color, 1.0);  
}
