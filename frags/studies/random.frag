
#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.14159265359
#define TWO_PI 6.28318530718

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_tex0;

// -------------------------- RANDOM ------------------------------
// if this random fails check out
// http://byteblacksmith.com/improvements-to-the-canonical-one-liner-glsl-rand-for-opengl-es-2-0/
float rand(vec2 st, float t){
    return fract(sin(dot(st.xy + fract(t*0.0013) ,vec2(12.9898,78.233))) * 43758.5453);
}

float rand( in float i, in float t ){
    return fract(sin(dot( vec2(i, t), vec2(12.9898,78.233))) * 43758.5453);
}

#define GRID 24.0
// ------------------- SHADER ----------------------------------
void main(){
  
  vec2 st = gl_FragCoord.xy/u_resolution.xy;
  
  st *= GRID;
  st = floor( st );
  
  float v = rand( st, floor(u_time * 9.0) ); 
  
  gl_FragColor = vec4( vec3(v), 1.0 );

}
