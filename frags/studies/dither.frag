
#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.1415926535897932384626433832795
#define TWO_PI 6.2831853071795864769252867665590

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_tex0;

float rand(vec2 st, float t){
    return fract(sin(dot(st.xy + fract(t*0.0013) ,vec2(12.9898,78.233))) * 43758.5453);
}

// ------------------- FUNCTIONS -------------------------------
float dither_a( float x, vec4 thresholds ){
	float p0 = step(0.875, fract(gl_FragCoord.x*0.125));
	p0 *= step(0.875, fract(gl_FragCoord.y*0.125));
	
	float p1 = step(0.75, fract(gl_FragCoord.x*0.25));
	p1 *= step(0.75, fract(gl_FragCoord.y*0.25));

	float p2 = step(0.5,fract(gl_FragCoord.x*0.5));
	p2 *= step(0.5,fract(gl_FragCoord.y*0.5));

	float s3 = step( thresholds[3], x );
	float s2 = step( thresholds[2], x ) - s3;
	float redux = s2 + s3;
	float s1 = step( thresholds[1], x ) - redux;
	redux += s1;
	float s0 = step( thresholds[0], x ) - redux;
	
	return s0 * p0 + s1*p1 + s2*p2 + s3*1.0;
}

float dither_b( float x, vec4 thresholds ){

	float px = gl_FragCoord.x*0.25 + step(0.5, fract(gl_FragCoord.y/8.0))*0.5;
	float p2 = step(0.75,fract(px));
	float p1 = p2 * step(0.25, fract(gl_FragCoord.y/4.0))*0.5;
	float p0 = p2 * step(0.75, fract(gl_FragCoord.y/4.0))*0.5;

	float s3 = step( thresholds[3], x );
	float s2 = step( thresholds[2], x ) - s3;
	float redux = s2 + s3;
	float s1 = step( thresholds[1], x ) - redux;
	redux += s1;
	float s0 = step( thresholds[0], x ) - redux;
	
	return s0 * p0 + s1*p1 + s2*p2 + s3*1.0;
}

float dither_b7( float x, float t0, float t1, float t2, float t3, float t4, float t5 ){
	float px = gl_FragCoord.x*0.25 + step(0.5, fract(gl_FragCoord.y/16.0))*0.5;
	float p4 = step(0.75,fract(px));

	float p3 = p4 * step(0.25, fract(gl_FragCoord.y/8.0))*0.5;
	float p2 = p4 * step(0.5, fract(gl_FragCoord.y/8.0))*0.5;
	float p1 = p4 * step(0.75, fract(gl_FragCoord.y/8.0))*0.5;
	float p0 = p4 * step(0.875, fract(gl_FragCoord.y/8.0))*0.5;

	float s5 = step( t5, x );
	float s4 = step( t4, x ) - s5;
	float redux = s5 + s4;
	float s3 = step( t3, x ) - redux;
	redux += s3;
	float s2 = step( t2, x ) - redux;
	redux += s2;
	float s1 = step( t1, x ) - redux;
	redux += s1;
	float s0 = step( t0, x ) - redux;
	
	return s0*p0 + s1*p1 + s2*p2 + s3*p3 + s4*p4 + s5*1.0;
}

float dither_c( float x, vec4 thresholds ){

	float p2 = step(0.75,fract(gl_FragCoord.x*0.25));
	float p1 = p2 * step(0.25, fract(gl_FragCoord.y/4.0))*0.5;
	float p0 = p2 * step(0.75, fract(gl_FragCoord.y/4.0))*0.5;

	float s3 = step( thresholds[3], x );
	float s2 = step( thresholds[2], x ) - s3;
	float redux = s2 + s3;
	float s1 = step( thresholds[1], x ) - redux;
	redux += s1;
	float s0 = step( thresholds[0], x ) - redux;
	
	return s0 * p0 + s1*p1 + s2*p2 + s3*1.0;
}

float dither_cr( float x, vec4 thresholds, float angle ){

	float sin_theta = sin(angle);
	float cos_theta = cos(angle);
	mat2 rot_mat = mat2( cos_theta,-sin_theta, sin_theta, cos_theta); 
	vec2 rot = gl_FragCoord.xy * rot_mat;

	float p2 = step(0.75,fract(rot.x*0.25));
	float p1 = p2 * step(0.25, fract(rot.y/4.0))*0.5;
	float p0 = p2 * step(0.75, fract(rot.y/4.0))*0.5;

	float s3 = step( thresholds[3], x );
	float s2 = step( thresholds[2], x ) - s3;
	float redux = s2 + s3;
	float s1 = step( thresholds[1], x ) - redux;
	redux += s1;
	float s0 = step( thresholds[0], x ) - redux;
	
	return s0 * p0 + s1*p1 + s2*p2 + s3*1.0;
}

float dither_d( float x, vec4 thresholds ){
	float p0 = step(0.75, fract(gl_FragCoord.x*0.25));
	p0 *= step(0.75, fract(gl_FragCoord.y*0.25));
	
	float p1 = step(0.5, fract(gl_FragCoord.x*0.25));
	p1 *= step(0.5, fract(gl_FragCoord.y*0.25));

	float p2 = step(0.25,fract(gl_FragCoord.x*0.25));
	p2 *= step(0.25,fract(gl_FragCoord.y*0.25));

	float s3 = step( thresholds[3], x );
	float s2 = step( thresholds[2], x ) - s3;
	float redux = s2 + s3;
	float s1 = step( thresholds[1], x ) - redux;
	redux += s1;
	float s0 = step( thresholds[0], x ) - redux;
	
	return s0 * p0 + s1*p1 + s2*p2 + s3*1.0;
}

float dither_e( float x, vec4 thresholds ){

	float theta = PI * 0.25;
	float sin_theta = sin(theta);
	float cos_theta = cos(theta);
	mat2 rot_mat = mat2( cos_theta,-sin_theta, sin_theta, cos_theta); 
	vec2 rot = gl_FragCoord.xy * rot_mat;

	float mx = mod(rot.x, 8.0);
	float my = mod(rot.y, 8.0);

	float p2 = step( 4.0, mx ) * step( 4.0, my );
	
	float p1 = p2 * step(1.0, mod(gl_FragCoord.y, 2.0));
	float p0 = p2 * step(3.0, mod(gl_FragCoord.y, 4.0));

	float s3 = step( thresholds[3], x );
	float s2 = step( thresholds[2], x ) - s3;
	float redux = s2 + s3;
	float s1 = step( thresholds[1], x ) - redux;
	redux += s1;
	float s0 = step( thresholds[0], x ) - redux;
	
	return s0 * p0 + s1*p1 + s2*p2 + s3*1.0;
}

float dither_f( float x, vec4 thresholds ){

	float theta = PI * 0.25;
	float sin_theta = sin(theta);
	float cos_theta = cos(theta);
	mat2 rot_mat = mat2( cos_theta,-sin_theta, sin_theta, cos_theta); 
	vec2 rot = gl_FragCoord.xy * rot_mat;
	float mx = mod(rot.x, 8.0);
	float my = mod(rot.y, 8.0);

	float p2 = step( 2.0, mx ) * step( 1.0, my );
	float p1 = step( 6.0, mx ) * step( 2.0, my );
	float p0 = step( 7.0, mx ) * step( 7.0, my );

	float s3 = step( thresholds[3], x );
	float s2 = step( thresholds[2], x ) - s3;
	float redux = s2 + s3;
	float s1 = step( thresholds[1], x ) - redux;
	redux += s1;
	float s0 = step( thresholds[0], x ) - redux;
	
	return s0 * p0 + s1*p1 + s2*p2 + s3*1.0;
}

// code ported from alex-charlton.com/posts/Dithering_on_the_GPU/
// adapted for running on GLSL ES 2 ----
float mod4 ( float x ){
	float ph = x * 0.25;
	ph = ph - floor( ph );
	return ph * 4.0;
}

// updated: mat4 doesn't have to be const 
mat4 dither_matrix = mat4( 
	vec4( 0,  8, 2, 10), 
	vec4( 12, 4, 14, 6), 
	vec4( 3, 11,  1, 9), 
	vec4( 15, 7, 13, 5)
)

float dither_4x4( float x, vec2 coord ){
	int ix = int(mod4(coord.x));
    int iy = int(mod4(coord.y));
    float d = dither_matrix[iy][ix] / 16.0;
    
    float closest = step( x, 0.5 );
    float distance = abs(closest - x);
	float branch = step( distance, d );

    return 2.0*branch*closest + 1.0 - closest - branch;
}

// -------------------------------------

float dither_r( float x, vec2 coord, float steps, float seed ){
	x = (ceil(x*steps) -2.0)/ (steps-2.0);
	float r = rand( vec2(coord.x, seed), coord.y);
	r = 1.0-r;
	r = r*r*r*r; // remove or add * to change brightness curve
	r = 1.0-r;
    return step( r, x );
}

// ------------------- SHADER ----------------------------------
void main(){
    vec2 st = gl_FragCoord.xy/u_resolution;
	//float a = dither_c( st.x, vec4( 0.2, 0.4, 0.6, 0.8 ) );
	//float a = dither_cr( st.x, vec4( 0.2, 0.4, 0.6, 0.8 ), 0.1*TWO_PI );
	//float a = dither_b7( st.x, 0.15, 0.3, 0.45, 0.6, 0.75, 0.9);
	float a = dither_4x4( st.x, gl_FragCoord.xy );
	//float a = dither_r( st.x, gl_FragCoord.xy, 8.0, u_time*0.00001 );
	//float a = dither_r( st.x*st.x, gl_FragCoord.xy, 32.0, u_time*0.00001 );
    gl_FragColor = vec4( vec3(a), 1.0 );
}
