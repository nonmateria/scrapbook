
#ifdef GL_ES
precision highp float;
#endif

#define PI 3.14159265359
#define TWO_PI 6.28318530718

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_tex0;

// ------------------- FUNCTIONS -------------------------------

// ---- http://www.iquilezles.org/www/articles/functions/functions.htm --------

float almostIdentity( float x, float m, float n ){
    if( x>m ) return x;

    float a = 2.0*n - m;
    float b = 2.0*m - 3.0*n;
    float t = x/m;

    return (a*t + b)*t*t + n;
}

float impulse( float x, float k ){
    float h = k*x;
    return h*exp(1.0-h);
}

float cubicPulse( float c, float w, float x ){
    x = abs(x - c);
    if( x>w ) return 0.0;
    x /= w;
    return 1.0 - x*x*(3.0-2.0*x);
}

float expStep( float x, float k, float n ){
    return exp( -k*pow(x,n) );
}

float gain(float x, float k) {
    float a = 0.5*pow(2.0*((x<0.5)?x:1.0-x), k);
    return (x<0.5)?a:1.0-a;
}

float parabola( float x, float k ){
    return pow( 4.0*x*(1.0-x), k );
}
	
float pcurve( float x, float a, float b ){
    float k = pow(a+b,a+b) / (pow(a,a)*pow(b,b));
    return k * pow( x, a ) * pow( 1.0-x, b );
}

float sinc( float x, float k ){
    float a = PI * (float(k)*x-1.0);
    return sin(a)/a;
}

// ------------------- SHADER ----------------------------------
void main(){
  
    vec2 st = gl_FragCoord.xy/u_resolution;

	float x = st.x;

	x = impulse( x, 0.45 );
 
    gl_FragColor = vec4( vec3(x), 1.0 );
}
