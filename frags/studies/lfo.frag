
#ifdef GL_ES
precision highp float;
#endif

#define PI 3.1415926535897932384626433832795
#define TWO_PI 6.2831853071795864769252867665590

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_tex0;

// ------------------- FUNCTIONS -------------------------------
float circle_sdf(vec2 st) { 
    return length(st-.5)*2.; 
}

// ---- LFO FUNCTIONS ----
float lfo_sin(  in float speed ){ return (sin(u_time*speed*TWO_PI)*0.5 + 0.5); }
float lfo_cos(  in float speed ){ return (cos(u_time*speed*TWO_PI)*0.5 + 0.5); }
float lfo_saw(  in float speed ){ return 1.0-fract(u_time*speed); }
float lfo_ramp( in float speed ){ return fract(u_time*speed); }
float lfo_tri(  in float speed ){ return abs( (fract(u_time*speed) * 2.0) - 1.0 ); }
float lfo_pulse( in float speed, float pulse ){ return step( pulse, fract(u_time*speed) ); }
float lfo_square( in float speed ){ return step( 0.5, fract(u_time*speed) ); }

// ------------------- SHADER ----------------------------------
void main(){
  
    vec2 st = gl_FragCoord.xy/u_resolution;

	float amp = lfo_tri( 0.25 );

	float c = circle_sdf( st );
	float s = step( c, 0.1 + amp * 0.2 );

    gl_FragColor = vec4( vec3(s), 1.0 );
}
