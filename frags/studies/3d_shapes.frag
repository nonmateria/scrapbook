// code from http://jamie-wong.com/2016/07/15/ray-marching-signed-distance-functions/
// and https://www.shadertoy.com/view/Xtd3z7

// SDF shapes from 
// https://iquilezles.org/www/articles/distfunctions/distfunctions.htm

#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.1415926535897932384626433832795
#define TWO_PI 6.2831853071795864769252867665590

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_tex0;

// ------------------- demo functions --------------------------------
float lfo_ramp(  in float speed ){ return fract(u_time*speed); }
float lfo_tri(  in float speed ){ return abs( (fract(u_time*speed) * 2.0) - 1.0 ); }

float sdOctogon( in vec2 p, in float r )
{
    const vec3 k = vec3(-0.9238795325, 0.3826834323, 0.4142135623 );
    p = abs(p);
    p -= 2.0*min(dot(vec2( k.x,k.y),p),0.0)*vec2( k.x,k.y);
    p -= 2.0*min(dot(vec2(-k.x,k.y),p),0.0)*vec2(-k.x,k.y);
    p -= vec2(clamp(p.x, -k.z*r, k.z*r), r);
    return length(p)*sign(p.y);
}

// -------------- RAYMARCHING BOILERPLATE ----------------------------

const int MAX_MARCHING_STEPS = 255;
const float MIN_DIST = 0.0;
const float MAX_DIST = 100.0;
const float EPSILON = 0.0001;

float sphereSDF(vec3 p) {
    return length(p) - 1.0;
}

float sceneSDF(vec3 samplePoint);

float shortestDistanceToSurface(vec3 eye, vec3 marchingDirection, float start, float end) {
    float depth = start;
    for (int i = 0; i < MAX_MARCHING_STEPS; i++) {
        float dist = sceneSDF(eye + depth * marchingDirection);
        if (dist < EPSILON) {
			return depth;
        }
        depth += dist;
        if (depth >= end) {
            return end;
        }
    }
    return end;
}
            
vec3 rayDirection(float fieldOfView, vec2 size, vec2 fragCoord) {
    vec2 xy = fragCoord - size / 2.0;
    float z = size.y / tan(radians(fieldOfView) / 2.0);
    return normalize(vec3(xy, -z));
}

vec3 estimateNormal(vec3 p) {
    return normalize(vec3(
        sceneSDF(vec3(p.x + EPSILON, p.y, p.z)) - sceneSDF(vec3(p.x - EPSILON, p.y, p.z)),
        sceneSDF(vec3(p.x, p.y + EPSILON, p.z)) - sceneSDF(vec3(p.x, p.y - EPSILON, p.z)),
        sceneSDF(vec3(p.x, p.y, p.z  + EPSILON)) - sceneSDF(vec3(p.x, p.y, p.z - EPSILON))
    ));
}


float phongContribForLight(float k_d, vec3 p, vec3 eye, vec3 lightPos) {
    vec3 N = estimateNormal(p);
    vec3 L = normalize(lightPos - p);
    vec3 V = normalize(eye - p);
    vec3 R = normalize(reflect(-L, N));
    
    float dotLN = dot(L, N);
    float dotRV = dot(R, V);

    // Light not visible from this point on the surface
    if (dotLN < 0.0) { 
        return 0.0;
    } 
    // Light reflection in opposite direction as viewer, 
    // apply only diffuse  component
    if (dotRV < 0.0) { 
        return (k_d * dotLN);
    }
    return (k_d * dotLN );
}

mat4 viewMatrix(vec3 eye, vec3 center, vec3 up) {
    // Based on gluLookAt man page
    vec3 f = normalize(center - eye);
    vec3 s = normalize(cross(f, up));
    vec3 u = cross(s, f);
    return mat4(
        vec4(s, 0.0),
        vec4(u, 0.0),
        vec4(-f, 0.0),
        vec4(0.0, 0.0, 0.0, 1)
    );
}

// ------------------- 3D SDFs ---------------------------------

// required for many primitives 
float dot2( in vec2 v ) { return dot(v,v); }
float dot2( in vec3 v ) { return dot(v,v); }
float ndot( in vec2 a, in vec2 b ) { return a.x*b.x - a.y*b.y; }

float sdSphere( vec3 p, float s ){
	return length(p)-s;
}

float sdBox( vec3 p, vec3 b ){
	vec3 q = abs(p) - b;
	return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
}

float sdRoundBox( vec3 p, vec3 b, float r ){
	vec3 q = abs(p) - b;
	return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0) - r;
}

float sdBoundingBox( vec3 p, vec3 b, float e ){
	p = abs(p  )-b;
	vec3 q = abs(p+e)-e;
	return min(min(
		length(max(vec3(p.x,q.y,q.z),0.0))+min(max(p.x,max(q.y,q.z)),0.0),
		length(max(vec3(q.x,p.y,q.z),0.0))+min(max(q.x,max(p.y,q.z)),0.0)),
		length(max(vec3(q.x,q.y,p.z),0.0))+min(max(q.x,max(q.y,p.z)),0.0));
}

float sdTorus( vec3 p, vec2 t ){
	vec2 q = vec2(length(p.xz)-t.x,p.y);
	return length(q)-t.y;
}

float sdCappedTorus(in vec3 p, in vec2 sc, in float ra, in float rb){
	p.x = abs(p.x);
	float k = (sc.y*p.x>sc.x*p.y) ? dot(p.xy,sc) : length(p.xy);
	return sqrt( dot(p,p) + ra*ra - 2.0*ra*k ) - rb;
}

float sdLink( vec3 p, float le, float r1, float r2 ){
	vec3 q = vec3( p.x, max(abs(p.y)-le,0.0), p.z );
	return length(vec2(length(q.xy)-r1,q.z)) - r2;
}

float sdCylinder( vec3 p, vec3 c ){
	return length(p.xz-c.xy)-c.z;
}

float sdCone( vec3 p, vec2 c, float h ){
  float q = length(p.xz);
  return max(dot(c.xy,vec2(q,p.y)),-h-p.y);
}

float sdPlane( vec3 p, vec3 n, float h ){
	// n must be normalized
	return dot(p,n) + h;
}

float sdHexPrism( vec3 p, vec2 h ){
	const vec3 k = vec3(-0.8660254, 0.5, 0.57735);
	p = abs(p);
	p.xy -= 2.0*min(dot(k.xy, p.xy), 0.0)*k.xy;
	vec2 d = vec2(
		length(p.xy-vec2(clamp(p.x,-k.z*h.x,k.z*h.x), h.x))*sign(p.y-h.x),
		p.z-h.y );
	return min(max(d.x,d.y),0.0) + length(max(d,0.0));
}

float sdTriPrism( vec3 p, vec2 h ){
	vec3 q = abs(p);
	return max(q.z-h.y,max(q.x*0.866025+p.y*0.5,-p.y)-h.x*0.5);
}

float sdCapsule( vec3 p, vec3 a, vec3 b, float r )
{
	vec3 pa = p - a, ba = b - a;
	float h = clamp( dot(pa,ba)/dot(ba,ba), 0.0, 1.0 );
	return length( pa - ba*h ) - r;
}

float sdVerticalCapsule( vec3 p, float h, float r ){
	p.y -= clamp( p.y, 0.0, h );
	return length( p ) - r;
}

float sdCappedCylinder( vec3 p, float h, float r ){
	vec2 d = abs(vec2(length(p.xz),p.y)) - vec2(h,r);
	return min(max(d.x,d.y),0.0) + length(max(d,0.0));
}

float sdRoundedCylinder( vec3 p, float ra, float rb, float h ){
	vec2 d = vec2( length(p.xz)-2.0*ra+rb, abs(p.y) - h );
	return min(max(d.x,d.y),0.0) + length(max(d,0.0)) - rb;
}

float sdCappedCone( vec3 p, float h, float r1, float r2 ){
	vec2 q = vec2( length(p.xz), p.y );
	vec2 k1 = vec2(r2,h);
	vec2 k2 = vec2(r2-r1,2.0*h);
	vec2 ca = vec2(q.x-min(q.x,(q.y<0.0)?r1:r2), abs(q.y)-h);
	vec2 cb = q - k1 + k2*clamp( dot(k1-q,k2)/dot2(k2), 0.0, 1.0 );
	float s = (cb.x<0.0 && ca.y<0.0) ? -1.0 : 1.0;
	return s*sqrt( min(dot2(ca),dot2(cb)) );
}

float sdSolidAngle(vec3 p, vec2 c, float ra){
	// c is the sin/cos of the angle
	vec2 q = vec2( length(p.xz), p.y );
	float l = length(q) - ra;
	float m = length(q - c*clamp(dot(q,c),0.0,ra) );
	return max(l,m*sign(c.y*q.x-c.x*q.y));
}

float sdRoundCone( vec3 p, float r1, float r2, float h ){
	vec2 q = vec2( length(p.xz), p.y );

	float b = (r1-r2)/h;
	float a = sqrt(1.0-b*b);
	float k = dot(q,vec2(-b,a));

	if( k < 0.0 ) return length(q) - r1;
	if( k > a*h ) return length(q-vec2(0.0,h)) - r2;
		
	return dot(q, vec2(a,b) ) - r1;
}

float sdEllipsoid( vec3 p, vec3 r ){
	float k0 = length(p/r);
	float k1 = length(p/(r*r));
	return k0*(k0-1.0)/k1;
}

float sdRhombus(vec3 p, float la, float lb, float h, float ra){
	p = abs(p);
	vec2 b = vec2(la,lb);
	float f = clamp( (ndot(b,b-2.0*p.xz))/dot(b,b), -1.0, 1.0 );
	vec2 q = vec2(length(p.xz-0.5*b*vec2(1.0-f,1.0+f))*sign(p.x*b.y+p.z*b.x-b.x*b.y)-ra, p.y-h);
	return min(max(q.x,q.y),0.0) + length(max(q,0.0));
}

float sdOctahedron( vec3 p, float s){
	p = abs(p);
	return (p.x+p.y+p.z-s)*0.57735027;
}

float sdPyramid( vec3 p, float h){
	float m2 = h*h + 0.25;

	p.xz = abs(p.xz);
	p.xz = (p.z>p.x) ? p.zx : p.xz;
	p.xz -= 0.5;

	vec3 q = vec3( p.z, h*p.y - 0.5*p.x, h*p.x + 0.5*p.y);

	float s = max(-q.x,0.0);
	float t = clamp( (q.y-0.5*p.z)/(m2+0.25), 0.0, 1.0 );

	float a = m2*(q.x+s)*(q.x+s) + q.y*q.y;
	float b = m2*(q.x+0.5*t)*(q.x+0.5*t) + (q.y-m2*t)*(q.y-m2*t);

	float d2 = min(q.y,-q.x*m2-q.y*0.5) > 0.0 ? 0.0 : min(a,b);

	return sqrt( (d2+q.z*q.z)/m2 ) * sign(max(q.z,-p.y));
}

float udTriangle( vec3 p, vec3 a, vec3 b, vec3 c ){
	vec3 ba = b - a; vec3 pa = p - a;
	vec3 cb = c - b; vec3 pb = p - b;
	vec3 ac = a - c; vec3 pc = p - c;
	vec3 nor = cross( ba, ac );

	return sqrt(
		(sign(dot(cross(ba,nor),pa)) +
		 sign(dot(cross(cb,nor),pb)) +
		 sign(dot(cross(ac,nor),pc))<2.0)
		 ?
		 min( min(
		 dot2(ba*clamp(dot(ba,pa)/dot2(ba),0.0,1.0)-pa),
		 dot2(cb*clamp(dot(cb,pb)/dot2(cb),0.0,1.0)-pb) ),
		 dot2(ac*clamp(dot(ac,pc)/dot2(ac),0.0,1.0)-pc) )
		 :
		 dot(nor,pa)*dot(nor,pa)/dot2(nor) );
}

float udQuad( vec3 p, vec3 a, vec3 b, vec3 c, vec3 d ){
	vec3 ba = b - a; vec3 pa = p - a;
	vec3 cb = c - b; vec3 pb = p - b;
	vec3 dc = d - c; vec3 pc = p - c;
	vec3 ad = a - d; vec3 pd = p - d;
	vec3 nor = cross( ba, ad );

	return sqrt(
		(sign(dot(cross(ba,nor),pa)) +
		 sign(dot(cross(cb,nor),pb)) +
		 sign(dot(cross(dc,nor),pc)) +
		 sign(dot(cross(ad,nor),pd))<3.0)
		 ?
		 min( min( min(
		 dot2(ba*clamp(dot(ba,pa)/dot2(ba),0.0,1.0)-pa),
		 dot2(cb*clamp(dot(cb,pb)/dot2(cb),0.0,1.0)-pb) ),
		 dot2(dc*clamp(dot(dc,pc)/dot2(dc),0.0,1.0)-pc) ),
		 dot2(ad*clamp(dot(ad,pd)/dot2(ad),0.0,1.0)-pd) )
		 :
		 dot(nor,pa)*dot(nor,pa)/dot2(nor) );
}

// ---- Platonic solids ----
// by decrooks on shadertoy
// https://www.shadertoy.com/view/MtV3Dy

//Golden mean and inverse -  for the icosohedron and dodecadron
#define PHI 1.6180339887
#define INV_PHI 0.6180339887

float plane( vec3 p, vec3 origin, vec3 normal ){ 
	return dot(p - origin,normal);   
}

float doubleplane( vec3 p, vec3 origin, vec3 normal ){ 
	return max(dot(p - origin,normal),dot(-p - origin,normal));   
}

//cube by iq
float sdCube( vec3 p, float d ){
	return length(max(abs(p) -d,0.0));
}

float sdTetrahedron( vec3 p, float d ){
	float dn = 1.0 / sqrt(3.0);

	//The tetrahedran is the intersection of four planes:
	float sd1 = plane(p,vec3(d,d,d) ,vec3(-dn,dn,dn)) ; 
	float sd2 = plane(p,vec3(d,-d,-d) ,vec3(dn,-dn,dn)) ;
	float sd3 = plane(p,vec3(-d,d,-d) ,vec3(dn,dn,-dn)) ;
	float sd4 = plane(p,vec3(-d,-d,d) ,vec3(-dn,-dn,-dn)) ;

	//max intersects shapes
	return max(max(sd1,sd2),max(sd3,sd4));
}

float sdAltOctahedron1( vec3 p, float d ){
	//The octahedron is the intersection of two dual tetrahedra.  
	float f0 = sdTetrahedron(p,d);
	float f1 = sdTetrahedron(-p,d);

	return max( f0, f1 );
}   

float sdAltOctahedron2( vec3 p, float d ){
	//Alternative construction of octahedran.
	//The same as for a terahedron, except intersecting double planes (the volume between two paralell planes). 
	float dn =1.0/sqrt(3.0);
	float sd1 = doubleplane(p,vec3(d,d,d) ,vec3(-dn,dn,dn)) ; 
	float sd2 = doubleplane(p,vec3(d,-d,-d) ,vec3(dn,-dn,dn)) ;
	float sd3 = doubleplane(p,vec3(-d,d,-d) ,vec3(dn,dn,-dn)) ;
	float sd4 = doubleplane(p,vec3(-d,-d,d) ,vec3(-dn,-dn,-dn)) ;

	return max(max(sd1,sd2),max(sd3,sd4)); 
}   

float sdDodecahedron( vec3 p, float d ){
	//Some vertices of the icosahedron.
	//The other vertices are cyclic permutations of these, plus the opposite signs.
	//We don't need the opposite sign because we are using double planes - two faces for the price of one. 
	vec3 v = normalize(vec3(0.0,1.0,PHI));
	vec3 w = normalize(vec3(0.0,1.0,-PHI));

	//The dodecahedron is dual to the icosahedron. The faces of one corespond to the vertices of the oyther.
	//So we can construct the dodecahedron by intersecting planes passing through the vertices of the icosohedran.
	float ds = doubleplane(p,d*v,v);
	//max == intesect objects
	ds = max(doubleplane(p,d*w,w),ds); 

	ds = max(doubleplane(p,d*v.zxy,v.zxy),ds);
	ds = max(doubleplane(p,d*v.yzx,v.yzx),ds);


	ds = max(doubleplane(p,d*w.zxy,w.zxy),ds);
	ds = max(doubleplane(p,d*w.yzx,w.yzx),ds);
	return ds; 
}   

float sdIcosahedron( vec3 p, float d ){
	float h=1.0/sqrt(3.0);

	//Same idea as above, using the vertices of the dodecahedron
	vec3 v1 = h* vec3(1.0,1.0,1.0);
	vec3 v2 = h* vec3(-1.0,1.0,1.0);
	vec3 v3 = h* vec3(-1.0,1.0,-1.0);
	vec3 v4 = h* vec3(1.0,1.0,-1.0);

	vec3 v5 = h* vec3(0.0,INV_PHI,PHI);
	vec3 v6 = h* vec3(0.0,INV_PHI,-PHI);

	float ds = doubleplane(p,d*v1,v1);
	//max == intesect objects
	ds = max(doubleplane(p,d*v2,v2),ds);
	ds = max(doubleplane(p,d*v3,v3),ds); 
	ds = max(doubleplane(p,d*v4,v4),ds);
	ds = max(doubleplane(p,d*v5,v5),ds); 
	ds = max(doubleplane(p,d*v6,v6),ds);

	//plus cyclic permutaions of v5 and v6:
	ds = max(doubleplane(p,d*v5.zxy,v5.zxy),ds); 
	ds = max(doubleplane(p,d*v5.yzx,v5.yzx),ds);
	ds = max(doubleplane(p,d*v6.zxy,v6.zxy),ds);
	ds = max(doubleplane(p,d*v6.yzx,v6.yzx),ds);

	return ds;
}

// ------------ operations -------------------------------------------
// for extrusion and revolutions you need those 2d SDF functions:
// https://www.iquilezles.org/www/articles/distfunctions2d/distfunctions2d.htm

// p.xy has to be used to generate the sdf_2d_value
float opExtrusion( in vec3 p, in float sdf_2d_value, in float h ){
	vec2 w = vec2( sdf_2d_value, abs(p.z) - h );
	return min(max(w.x,w.y),0.0) + length(max(w,0.0));
}

// the returning vec2 should be used as 2d sdf input
vec2 opRevolution( in vec3 p, float o ){
	vec2 q = vec2( length(p.xz) - o, p.y );
	return q;
}

vec3 opTwist( in vec3 p, float amt ){
	float c = cos(amt*p.y);
	float s = sin(amt*p.y);
	mat2  m = mat2(c,-s,s,c);
	vec3  q = vec3(m*p.xz,p.y);
	return q.xzy;
}

vec3 opCheapBend( in vec3 p, float amt ){
    float c = cos(amt*p.x);
    float s = sin(amt*p.x);
    mat2  m = mat2(c,-s,s,c);
    vec3  q = vec3(m*p.xy,p.z);
    return q;
}

vec3 opRep( in vec3 p, in vec3 c ){
    vec3 q = mod(p+0.5*c,c)-0.5*c;
    return q;
}

vec3 opRepLim( in vec3 p, in float c, in vec3 l ){
	// floor(x + 0.5) instead of round(x)
    vec3 q = p-c*clamp(floor( 0.5 + p/c ),-l,l);
    return q;
}

// ------------------- ROTATE FUNCTIONS ------------------------
vec3 rotateX(vec3 p, float theta) {
    float c = cos(theta);
    float s = sin(theta);

    mat4 m = mat4(
        vec4(1.0, 0.0, 0.0, 0.0),
        vec4(0.0, c,   -s,  0.0),
        vec4(0.0, s,   c,   0.0),
        vec4(0.0, 0.0, 0.0, 1.0)
    );

    vec4 p4 = m * vec4(p, 1.0);
    return p4.xyz;
}

vec3 rotateY(vec3 p, float theta) {
    float c = cos(theta);
    float s = sin(theta);

    mat4 m = mat4(
        vec4(c,   0.0, s,   0.0),
        vec4(0.0, 1.0, 0.0, 0.0),
        vec4(-s,  0.0, c,   0.0),
        vec4(0.0, 0.0, 0.0, 1.0)
    );
    
    vec4 p4 = m * vec4(p, 1.0);
    return p4.xyz;
}

vec3 rotateZ( vec3 p, float theta) {
    float c = cos(theta);
    float s = sin(theta);

    mat4 m = mat4(
        vec4(c,   -s,  0.0, 0.0),
        vec4(s,   c,   0.0, 0.0),
        vec4(0.0, 0.0, 1.0, 0.0),
        vec4(0.0, 0.0, 0.0, 1.0)
    );
    vec4 p4 = m * vec4(p, 1.0);
    return p4.xyz;
}

vec3 rotate(vec3 p, vec3 ax, float ro) {
  return mix(dot(ax, p)*ax, p, cos(ro)) + cross(ax,p)*sin(ro);
}

// ------------------- SHADER ----------------------------------
void main(){
	// ------ dither patterns ------
	float dark_pattern = step( fract( gl_FragCoord.x/4.0 ), 0.25); 
	dark_pattern *= step( fract( gl_FragCoord.y/4.0 ), 0.25); 

	float mid_pattern = step( fract( gl_FragCoord.x/4.0 ), 0.25);

	float light_pattern = 1.0; 
 
	// ------ raymarching parameters ------
	float distance = 70.0;
    float K_a = 0.05; // ambient gradient
    float K_d = 0.9; // light gradient
    vec3 light_pos = vec3(2.0, 4.0, 4.0 );

	float clok = u_time * 0.2;
	float theta = lfo_ramp( 0.03 ) * TWO_PI;
    vec3 camera = vec3( 7.0, 3.0, 7.0);
   
    vec3 look_at = vec3( 0, 0, 0 );

	// ------ more raymarching boilerplate ------
    vec2 st = gl_FragCoord.xy/u_resolution;
	vec3 viewDir = rayDirection(distance, u_resolution.xy, gl_FragCoord.xy);  
    mat4 viewToWorld = viewMatrix(camera, look_at, vec3(0.0, 1.0, 0.0));
    vec3 worldDir = (viewToWorld * vec4(viewDir, 0.0)).xyz;
    float dist = shortestDistanceToSurface(camera, worldDir, MIN_DIST, MAX_DIST);
    if (dist > MAX_DIST - EPSILON) {
        // Didn't hit anything
        gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0); // opaque
        //gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0); // transparent
		return;
    }
    // The closest point on the surface to the eyepoint along the view ray
    vec3 p = camera + dist * worldDir;
      
    float c = K_a;
    c += phongContribForLight(K_d, p, camera, light_pos );

	gl_FragColor = vec4(vec3(c), 1.0);
}

float sdHexagon( in vec2 p, in float r )
{
    const vec3 k = vec3(-0.866025404,0.5,0.577350269);
    p = abs(p);
    p -= 2.0*min(dot(k.xy,p),0.0)*k.xy;
    p -= vec2(clamp(p.x, -k.z*r, k.z*r), r);
    return length(p)*sign(p.y);
}

float sceneSDF(vec3 samplePoint) {
	float sdf = 0.0;
	float s0 = 0.0;
	float s1 = 0.0;
	
	vec3 p = rotateY(samplePoint.xyz, -u_time*0.5);

	//p = opTwist( p, 10.0 );
	
	//s0 = sdSphere( p, 0.5 );
	//s0 = sdBox(p, vec3( 0.5, 1.5, 0.5 ) );
	//s0 = sdRoundBox(p, vec3( 0.5, 1.5, 0.5 ), 0.2 );
	//s0 = sdBoundingBox(p, vec3( 0.5, 1.5, 0.5 ), 0.05 );
	//s0 = sdTorus( p, vec2( 1.8, 0.15 ) );
	//s0 = sdCappedTorus( p, vec2( 1.8, 0.15 ), 0.85, 0.2  );
	//s0 = sdLink( p, 1.8, 0.5, 0.05) ;
	//s0 = sdCylinder( p, vec3(0.0, 0.0, 0.4) ); // ???
	//s0 = sdCone( vec3(rotated.x,rotated.y-1.1, rotated.z ), vec2( 0.6, 0.2 ), 2.5 );
	//s0 = sdPlane(p, vec3( 0.2, 1.0, 0.5 ), 4.25 );
	//s0 = sdHexPrism( p, vec2( 1.8, 0.15 ) );	
	//s0 = sdTriPrism( p, vec2( 1.8, 0.15 ) );	
	//s0 = sdVerticalCapsule( p, 1.2, 0.15) ;	
	//s0 = sdCappedCylinder( p, 0.3, 1.15) ;	
	//s0 = sdRoundedCylinder( p, 1.3, 0.2, 0.35) ;	
	//s0 = sdCappedCone( p, 1.1, 0.8, 0.35) ;	
	//s0 = sdRoundCone( p, 0.75, 0.1, 1.95) ;	
	//s0 = sdSolidAngle( p, vec2 ( 1.1, 0.9), 1.5) ;
	//s0 = sdEllipsoid(p, vec3( 0.5, 1.8, 0.8 ) );	
	//s0 = sdRhombus( p, 1.5, 0.5, 0.5, 0.0);	
	//s0 = sdPyramid( p, 1.1 );	
	
	//s0 = sdCube( p, 1.0 );
	//s0 = sdTetrahedron( p, 1.0 );
	//s0 = sdOctahedron( p, 1.0 );
	s0 = sdDodecahedron( p, 1.0 );
	//s0 = sdIcosahedron( p, 1.0 );

	//s0 = s0 -0.1; // round
	
	//p = opRep(rotated.xyz, vec3( 2.0, 2.0, 4.0 ) );
	//p = opRepLim( rotated.xyz, 0.75, vec3( 2.0 ) );
	//s0 = sdBox(p, vec3( 0.15, 0.15, 0.15 ) );
	//s0 = sdOctahedron(p, 0.2 );
	//s0 = sdEllipsoid(p, vec3(0.1, 0.18, 0.1 ) );
	
	//float s2d = sdOctogon( p.xy, 1.2 );
	//s0 = opExtrusion( p, s2d, 0.25 );
	//s2d = sdOctogon( p.xy, 1.0 );
	//s1 = opExtrusion( p, s2d, 0.28 );
	
	sdf = s0;
	//sdf = min( s0, s1 ); // union 
	//sdf = max( -s1, s0 ); // subtraction 
	//sdf = max( s0, s1 ); // intersection 

	return sdf;    
}
