
// thanks to this article on hackaday:
// https://hackaday.com/2021/04/13/alien-art-drawn-with-surprisingly-simple-math/

// and this comment on twitter:
// https://twitter.com/atesgoral/status/894025019956748289

#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.1415926535897932384626433832795
#define TWO_PI 6.2831853071795864769252867665590

uniform vec2 u_resolution;
uniform float u_time;

// ------------------- FUNCTIONS -------------------------------

float xb(float a, float b, float bit) {
	float e = exp2(bit);
	float m = e * 2.0;
	return mod(a, m) >= e ^^ mod(b, m) >= e ? e : 0.0; 
}

float xor(float a, float b) {
	return xb(a, b, 0.0)
	// hint : try comment and decomment next lines
	+ xb(a, b, 1.0)
	+ xb(a, b, 2.0)
	+ xb(a, b, 3.0)
	+ xb(a, b, 4.0)
	+ xb(a, b, 5.0)
	+ xb(a, b, 6.0)
	+ xb(a, b, 7.0)
	+ xb(a, b, 8.0)
	//+ xb(a, b, 9.0)
	+ xb(a, b, 10.0)
	//+ xb(a, b, 11.0)
	//+ xb(a, b, 12.0)
	;
}

// ------------------- SHADER ----------------------------------
void main(){  
	vec2 st = gl_FragCoord.xy;

	st *= 0.5;
	//st.x*=12.0;

	//st.y += floor( u_time * 42.5 );

	float t = fract(u_time/32.0);
	t = abs( (t * 2.0) - 1.0 );

	st.x = 1.0-st.x;
	float alpha = mod( xor(st.x, st.y ), 9.0 + t*0.125 );

	//float alpha = mod( xor(st.x, st.y ), 9.0 );

	gl_FragColor = vec4( vec3(1.0-alpha), 1.0 );
	//gl_FragColor = vec4( vec3(1.0), 1.0-alpha );
}
