// ray marching code from 
// http://jamie-wong.com/2016/07/15/ray-marching-signed-distance-functions/
// https://www.shadertoy.com/view/Xtd3z7

// SDF shapes from 
// https://iquilezles.org/www/articles/distfunctions/distfunctions.htm

#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.1415926535897932384626433832795
#define TWO_PI 6.2831853071795864769252867665590

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_tex0;


// code ported from alex-charlton.com/posts/Dithering_on_the_GPU/
// adapted for running on GLSL ES 2 ----
const mat4 dither_matrix = mat4( 
		vec4( 0,  8, 2, 10), 
		vec4( 12, 4, 14, 6), 
		vec4( 3, 11,  1, 9), 
		vec4( 15, 7, 13, 5)
);
float dither_4x4( float x, vec2 coord ){
	int ix = int(mod(coord.x, 4.0));
    int iy = int(mod(coord.y, 4.0));
    float d = dither_matrix[iy][ix] / 16.0;
    
    float closest = step( x, 0.5 );
    float distance = abs(closest - x);
	float branch = step( distance, d );

    return 2.0*branch*closest + 1.0 - closest - branch;
}
// -------------------------------------


// -------------- RAYMARCHING BOILERPLATE ---------------------------------------

const int MAX_MARCHING_STEPS = 255;
const float MIN_DIST = 0.0;
const float MAX_DIST = 100.0;
const float EPSILON = 0.0001;

float sphereSDF(vec3 p) {
    return length(p) - 1.0;
}

float sceneSDF(vec3 samplePoint);

float shortestDistanceToSurface(vec3 eye, vec3 marchingDirection, float start, float end) {
    float depth = start;
    for (int i = 0; i < MAX_MARCHING_STEPS; i++) {
        float dist = sceneSDF(eye + depth * marchingDirection);
        if (dist < EPSILON) {
			return depth;
        }
        depth += dist;
        if (depth >= end) {
            return end;
        }
    }
    return end;
}
            
vec3 rayDirection(float fieldOfView, vec2 size, vec2 fragCoord) {
    vec2 xy = fragCoord - size / 2.0;
    float z = size.y / tan(radians(fieldOfView) / 2.0);
    return normalize(vec3(xy, -z));
}

vec3 estimateNormal(vec3 p) {
    return normalize(vec3(
        sceneSDF(vec3(p.x + EPSILON, p.y, p.z)) - sceneSDF(vec3(p.x - EPSILON, p.y, p.z)),
        sceneSDF(vec3(p.x, p.y + EPSILON, p.z)) - sceneSDF(vec3(p.x, p.y - EPSILON, p.z)),
        sceneSDF(vec3(p.x, p.y, p.z  + EPSILON)) - sceneSDF(vec3(p.x, p.y, p.z - EPSILON))
    ));
}


float phongContribForLight(float k_d, vec3 p, vec3 eye, vec3 lightPos) {
    vec3 N = estimateNormal(p);
    vec3 L = normalize(lightPos - p);
    vec3 V = normalize(eye - p);
    vec3 R = normalize(reflect(-L, N));
    
    float dotLN = dot(L, N);
    float dotRV = dot(R, V);

    // Light not visible from this point on the surface
    if (dotLN < 0.0) { 
        return 0.0;
    } 
    // Light reflection in opposite direction as viewer, 
    // apply only diffuse  component
    if (dotRV < 0.0) { 
        return (k_d * dotLN);
    }
    return (k_d * dotLN );
}

mat4 viewMatrix(vec3 eye, vec3 center, vec3 up) {
    // Based on gluLookAt man page
    vec3 f = normalize(center - eye);
    vec3 s = normalize(cross(f, up));
    vec3 u = cross(s, f);
    return mat4(
        vec4(s, 0.0),
        vec4(u, 0.0),
        vec4(-f, 0.0),
        vec4(0.0, 0.0, 0.0, 1)
    );
}

// ------------------- FUNCTIONS -------------------------------
float lfo_ramp(  in float speed ){ return fract(u_time*speed); }
float lfo_tri(  in float speed ){ return abs( (fract(u_time*speed) * 2.0) - 1.0 ); }

// hash based 3d value noise
// function taken from https://www.shadertoy.com/view/XslGRr
// Created by inigo quilez - iq/2013
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

float vnhash( float n )
{
    return fract(sin(n)*43758.5453);
}

float vnoise( vec3 x )
{
    // The noise function returns a value in the range -1.0f -> 1.0f

    vec3 p = floor(x);
    vec3 f = fract(x);

    f       = f*f*(3.0-2.0*f);
    float n = p.x + p.y*57.0 + 113.0*p.z;

    return mix(mix(mix( vnhash(n+0.0), vnhash(n+1.0),f.x),
                   mix( vnhash(n+57.0), vnhash(n+58.0),f.x),f.y),
               mix(mix( vnhash(n+113.0), vnhash(n+114.0),f.x),
                   mix( vnhash(n+170.0), vnhash(n+171.0),f.x),f.y),f.z);
}


// ------------------- 3D SDFs ---------------------------------

float sdBox( vec3 p, vec3 b )
{
  vec3 q = abs(p) - b;
  return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
}

float sdOctahedron( vec3 p, float s)
{
  p = abs(p);
  return (p.x+p.y+p.z-s)*0.57735027;
}

vec3 rotateY(vec3 p, float theta) {
    float c = cos(theta);
    float s = sin(theta);

    mat4 m = mat4(
        vec4(c,   0.0, s,   0.0),
        vec4(0.0, 1.0, 0.0, 0.0),
        vec4(-s,  0.0, c,   0.0),
        vec4(0.0, 0.0, 0.0, 1.0)
    );
    
    vec4 p4 = m * vec4(p, 1.0);
    return p4.xyz;
}

vec3 op_limited_repeat( in vec3 p, in float c, in vec3 l ){
	// floor(x + 0.5) instead of round(x)
    vec3 q = p-c*clamp(floor( 0.5 + p/c ),-l,l);
    return q;
}

// ------------------- SHADER ----------------------------------

float sceneSDF(vec3 samplePoint) {
	float sdf = 0.0f;
	vec3 sz = vec3( 0.2, 1.8, 0.03);

	vec3 rotated = rotateY(samplePoint.xyz, -u_time*0.3);

	float sep = 0.8;

	vec3 limit = vec3(2, 2, 2 );
	vec3 id = floor( 0.5 + rotated/sep );
	vec3 pt = rotated-sep*clamp(id,-limit,limit);

	float mod = 0.0;
	//vec3 p = op_limited_repeat(rotated, sep, vec3(2, 2, 2 ));

	float ndx = 1.0;
	mod = vnoise( vec3(id.x*ndx, id.y*ndx + u_time*1.2, id.z*ndx));

	mod = abs( id.x + id.y + id.z ) * 0.25;
	
	//float o0 = sdOctahedron( p,  mod );
	float o0 = sdBox( pt,  vec3( mod * 0.1 ) );

	sdf = o0;

	return sdf;    
}

void main(){
	// ------ dither patterns ------
	float dark_pattern = step( fract( gl_FragCoord.x/4.0 ), 0.25); 
	dark_pattern *= step( fract( gl_FragCoord.y/4.0 ), 0.25); 

	float mid_pattern = step( fract( gl_FragCoord.x/4.0 ), 0.25);

	float light_pattern = 1.0; 
 
	// ------ raymarching parameters ------
	float distance = 70.0;
    float K_a = 0.05; // ambient gradient
    float K_d = 0.9; // light gradient
    vec3 light_pos = vec3(2.0, 4.0, 4.0 );

	float clok = u_time * 0.2;
	float theta = lfo_ramp( 0.03 ) * TWO_PI;
    vec3 camera = vec3( 7.0, 3.0, 7.0);
   
    vec3 look_at = vec3( 0, 0, 0 );

	// ------ more raymarching boilerplate ------
    vec2 st = gl_FragCoord.xy/u_resolution;
	vec3 viewDir = rayDirection(distance, u_resolution.xy, gl_FragCoord.xy);  
    mat4 viewToWorld = viewMatrix(camera, look_at, vec3(0.0, 1.0, 0.0));
    vec3 worldDir = (viewToWorld * vec4(viewDir, 0.0)).xyz;
    float dist = shortestDistanceToSurface(camera, worldDir, MIN_DIST, MAX_DIST);
    if (dist > MAX_DIST - EPSILON) {
        // Didn't hit anything
        gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
		return;
    }
    // The closest point on the surface to the eyepoint along the view ray
    vec3 p = camera + dist * worldDir;
      
    float c = K_a;
    c += phongContribForLight(K_d, p, camera, light_pos );

	gl_FragColor = vec4(vec3(c), 1.0);

	// dithering 
    //c *= 1.4;
	//float a = dither_4x4( c, gl_FragCoord.xy );
	//gl_FragColor = vec4(vec3(a), 1.0);
}