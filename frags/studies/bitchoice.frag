
#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;

void main (void) {
    vec2 st = gl_FragCoord.xy/u_resolution;
    
    float val = st.x;

    float index = floor( st.y*6.0 );

    float bitsel = pow( 2.0, index );

    float choice = step( 0.5, fract( val * bitsel ) );

    gl_FragColor = vec4( vec3(choice), 1.0 );
}
