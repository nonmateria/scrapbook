
#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec2 u_resolution;
uniform sampler2D u_tex0;

void main (void) {
    vec2 st = gl_FragCoord.xy/u_resolution;
        
    float undef;
    
    gl_FragColor = vec4( vec3(undef), 1.0 );
}
