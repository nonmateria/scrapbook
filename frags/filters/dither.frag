
#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.1415926535897932384626433832795
#define TWO_PI 6.2831853071795864769252867665590

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_tex0;

// code ported from alex-charlton.com/posts/Dithering_on_the_GPU/
// adapted for running on GLSL ES 2 ----
const mat4 dither_matrix = mat4( 
		vec4( 0,  8, 2, 10), 
		vec4( 12, 4, 14, 6), 
		vec4( 3, 11,  1, 9), 
		vec4( 15, 7, 13, 5)
);
float dither_4x4( float x, vec2 coord ){
	int ix = int(mod(coord.x, 4.0));
    int iy = int(mod(coord.y, 4.0));
    float d = dither_matrix[iy][ix] / 16.0;
    
    float closest = step( x, 0.5 );
    float distance = abs(closest - x);
	float branch = step( distance, d );

    return 2.0*branch*closest + 1.0 - closest - branch;
}
// -------------------------------------

void main(){
    vec2 st = gl_FragCoord.xy/u_resolution;
    vec4 source = texture2D( u_tex0, st ); // for texture access
    
    vec3 color = source.rgb;
    float luminance = color.r*0.299 + color.g*0.587 + color.b*0.114;	

    float low = 0.2;
    float high = 0.7;    
    float alpha = smoothstep( low, high, luminance );
    
    alpha = dither_4x4( alpha, gl_FragCoord.xy );
    
    gl_FragColor = vec4( vec3(alpha), 1.0 );
}
