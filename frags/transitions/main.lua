
-- boilerplat to port and test transitions from 
-- https://gl-transitions.com/gallery

require "strict"

----------------------------------------------------
window.size( 480, 480 )
layer.create( "def")

frag.load( "transitions.frag" )

png.load( "frames", "sigils" ) 

----------------------------------------------------
function loop()
	layer.clear(0,0,0,255)

	frag.select( "transitions" ) 
		frag.uniform( "progress", fn.map(mouse.x(),50, 480-50) ) 
		png.uniform( "u_from", "sigils", 0 )
		png.uniform( "u_to", "sigils", 1 )
	frag.apply()
end
