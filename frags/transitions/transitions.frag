
// boilerplat to port and test transitions from 
// https://gl-transitions.com/gallery

#ifdef GL_ES
precision mediump float;
#endif

// ------------------- BOILERPLATE -------------------------------
uniform sampler2D u_from;
uniform sampler2D u_to;
uniform float progress;
vec4 getFromColor( vec2 uv ){ return texture2D( u_from, uv ); }
vec4 getToColor( vec2 uv ){ return texture2D( u_to, uv ); }

// ------------------- TRANSITIONS --------------------------------

// MORPH -------------------------------------
// Author: paniq
// License: MIT

const float strength = 0.05;
vec4 morph(vec2 p) {
  vec4 ca = getFromColor(p);
  vec4 cb = getToColor(p);
  
  vec2 oa = (((ca.rg+ca.b)*0.5)*2.0-1.0);
  vec2 ob = (((cb.rg+cb.b)*0.5)*2.0-1.0);
  vec2 oc = mix(oa,ob,0.5)*strength;
  
  float w0 = progress;
  float w1 = 1.0-w0;
  return mix(getFromColor(p+oc*w0), getToColor(p-oc*w1), progress);
}

// CROSSWARP ---------------------------------
// Author: Eke Péter <peterekepeter@gmail.com>
// License: MIT

vec4 crosswarp(vec2 p) {
  float x = progress;
  x=smoothstep(.0,1.0,(x*2.0+p.x-1.0));
  return mix(getFromColor((p-.5)*(1.-x)+.5), getToColor((p-.5)*x+.5), x);
}

// FLYEYE ------------------------------------
// Author: gre
// License: MIT
const float size = 0.04;
const float zoom = 50.0;
const float colorSeparation = 0.3;

vec4 flyeye(vec2 p) {
  float inv = 1. - progress;
  vec2 disp = size*vec2(cos(zoom*p.x), sin(zoom*p.y));
  vec4 texTo = getToColor(p + inv*disp);
  vec4 texFrom = vec4(
    getFromColor(p + progress*disp*(1.0 - colorSeparation)).r,
    getFromColor(p + progress*disp).g,
    getFromColor(p + progress*disp*(1.0 + colorSeparation)).b,
    1.0);
  return texTo*progress + texFrom*inv;
}

// WIND --------------------------------------
// Author: gre
// License: MIT

// Custom parameters
const float wsize = 0.2;

float rand (vec2 co) {
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec4 wind (vec2 uv) {
  float r = rand(vec2(0, uv.y));
  float m = smoothstep(0.0, -size, uv.x*(1.0-wsize) + size*r - (progress * (1.0 + size)));
  return mix(
    getFromColor(uv),
    getToColor(uv),
    m
  );
}

// DIRECTIONAL -------------------------------
// Author: Gaëtan Renaudeau
// License: MIT

const vec2 direction = vec2(-1.0, 0.0);

vec4 directional (vec2 uv) {
  vec2 p = uv + progress * sign(direction);
  vec2 f = fract(p);
  return mix(
    getToColor(f),
    getFromColor(f),
    step(0.0, p.y) * step(p.y, 1.0) * step(0.0, p.x) * step(p.x, 1.0)
  );
}

// -------------------------------------------

// ------------------- TEST CODE ---------------------------------
uniform vec2 u_resolution;

void main(){
    vec2 st = gl_FragCoord.xy/u_resolution;
	st.y = 1.0 - st.y;

    vec4 source = wind(st); 

    gl_FragColor = source;
}
