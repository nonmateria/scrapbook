
#ifdef GL_ES
precision highp float;
#endif

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_buffer;
varying vec2 st;

float rand(vec2 st, float t){
    return fract(sin(dot(st.xy + fract(t*0.0013) ,vec2(12.9898,78.233))) * 43758.5453);
}

// variables : -------------

float u_feedback = 0.99;
vec3 u_background = vec3( 0, 0, 0 );
float u_displace = 0.01;
   
// -------------------------
void main (void) {
    vec2 st = gl_FragCoord.xy/u_resolution;
    float rat = u_resolution.x / u_resolution.y;
    
    float nox = rand( st, u_time ) - 0.5;
    float noy = rand( st+1.0, u_time ) - 0.5;

    st.x -= u_displace * nox;
    st.y -= u_displace * noy * rat;

    //st.y -= 0.001;
    
    vec4 source = texture2D( u_buffer, st );

    vec3 color = source.rgb*u_feedback + u_background*(1.0-u_feedback);

    gl_FragColor = vec4( color, 1.0 );
}
