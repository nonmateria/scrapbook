
// wolfram automata

#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec2 u_resolution;
uniform sampler2D u_buffer;

// reassign those as uniform for interaction 
float u_direction = -1.0;
float u_feedback = 0.9;
float u_rule = 2. + 4. + 8. + 16. + 64.;

void main (void) {
    vec2 st = gl_FragCoord.xy/u_resolution;
    vec2 dt = 1.0 / u_resolution.xy;
    dt.y = dt.y * u_direction;

	float n0 = texture2D(u_buffer, vec2( st.x-dt.x, st.y+dt.y)).r;
	float n1 = texture2D(u_buffer, vec2( st.x, st.y+dt.y)).r;
	float n2 = texture2D(u_buffer, vec2( st.x+dt.x, st.y+dt.y)).r;
	
	n0 = step(0.5, n0) * 4.0;
	n1 = step(0.5, n1) * 2.0;
	n2 = step(0.5, n2);
	float sum = n0 + n1 + n2;

	float rule = 110.0;
	float bit = floor(u_rule / pow(2.0, sum )) ;
	bit = step(0.5, mod( bit, 2.0 ));

	float a = texture2D(u_buffer, st).r * u_feedback;
	
	float c = bit + a;
	
    gl_FragColor = vec4( vec3(c), 1.0 );
}
