
#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec2 u_resolution;
uniform sampler2D u_buffer;

float rand(vec2 st, float t){
    return fract(sin(dot(st.xy + fract(t*0.0013) ,vec2(12.9898,78.233))) * 43758.5453);
}

void main (void) {
	float u_seed = floor(u_time*3.0); // automate this on triggers
	float u_feedback = 0.99;
	
    vec2 st = gl_FragCoord.xy/u_resolution;
    
    float pct = 0.017;
    
    float nox = rand( st, u_seed ) - 0.5;
    float noy = rand( st, u_seed + 1.0 ) - 0.5;
    
    st.x -= pct * nox;
    st.y -= pct * noy;

	//st.y -= 0.002;

    vec4 source = texture2D( u_buffer, st );

    vec3 color = source.rgb * u_feedback;

    gl_FragColor = vec4( color, 1.0 );
}
