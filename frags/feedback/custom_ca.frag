
// 2d cellular automata, with various rulesets  

#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec2 u_resolution;
uniform sampler2D u_buffer;

const mat4 rules = mat4( 
	vec4( 0, 0, 0, 1), vec4( 0, 0, 0, 0 ),  // birth
	vec4( 0, 1, 1, 1), vec4( 0, 1, 0, 0)    // death 
);

void main (void) {
    vec2 st = gl_FragCoord.xy/u_resolution;
	float sx = 1.0 / u_resolution.x;
	float sy = 1.0 / u_resolution.y;

	float n0 = texture2D(u_buffer, vec2( st.x+sx, st.y-sy)).r;
	float n1 = texture2D(u_buffer, vec2( st.x+sx, st.y)).r;
	float n2 = texture2D(u_buffer, vec2( st.x+sx, st.y+sy)).r;
	float n3 = texture2D(u_buffer, vec2( st.x-sx, st.y-sy)).r;
	float n4 = texture2D(u_buffer, vec2( st.x-sx, st.y)).r;
	float n5 = texture2D(u_buffer, vec2( st.x-sx, st.y+sy)).r;
	float n6 = texture2D(u_buffer, vec2( st.x, st.y-sy)).r;
	float n7 = texture2D(u_buffer, vec2( st.x, st.y+sy)).r;

	float sum = min(n0+n1+n2+n3+n4+n5+n6+n7, 7.0); 

	float a = texture2D(u_buffer, st).r;

	sum += 8.0 * step( 0.5, a );

	int ix = int(mod(sum,4.0));
	int iy = int(sum/4.0);

    gl_FragColor = vec4( vec3( rules[iy][ix]), 1.0 );
}
