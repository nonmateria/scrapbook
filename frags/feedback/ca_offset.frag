
// 2d cellular automata, with various rulesets  

#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec2 u_resolution;
uniform sampler2D u_buffer;


const mat4 flames = mat4( 
	vec4( 0, 0, 0, 1), vec4( 1, 0, 0, 0 ),  // birth
	vec4( 0, 1, 0, 1), vec4( 0, 1, 0, 0)    // death 
);

const mat4 disappear = mat4( 
	vec4( 0, 0, 0, 1), vec4( 0, 0, 0, 0 ),  // birth
	vec4( 0, 0, 0, 0), vec4( 1, 1, 0, 0)    // death 
);

const mat4 evaporate = mat4( 
	vec4( 0, 0, 0, 1), vec4( 0, 0, 0, 0 ),  // birth
	vec4( 0, 0, 0, 1), vec4( 1, 1, 0, 0)    // death 
);

const mat4 corals = mat4( 
	vec4( 0, 0, 0, 1), vec4( 0, 0, 0, 0 ),  // birth
	vec4( 0, 1, 1, 1), vec4( 1, 1, 1, 1)    // death 
);

const mat4 mazes = mat4( 
	vec4( 0, 0, 0, 1), vec4( 0, 0, 0, 0 ),  // birth
	vec4( 0, 1, 1, 1), vec4( 1, 1, 0, 0)    // death 
);

const mat4 custom = mat4( 
	vec4( 0, 0, 0, 1), vec4( 0, 0, 0, 0 ),  // birth
	vec4( 0, 1, 1, 1), vec4( 1, 1, 0, 0)    // death 
);

#define RULES mazes
vec2 u_direction = vec2( 0.0, -1.0 );

void main (void) {
    vec2 st = gl_FragCoord.xy/u_resolution;

    vec2 dt = 1.0 / u_resolution.xy;
	vec2 off = st + u_direction * dt;

	float n0 = texture2D(u_buffer, vec2( off.x+dt.x, off.y-dt.y)).r;
	float n1 = texture2D(u_buffer, vec2( off.x+dt.x, off.y)).r;
	float n2 = texture2D(u_buffer, vec2( off.x+dt.x, off.y+dt.y)).r;
	float n3 = texture2D(u_buffer, vec2( off.x-dt.x, off.y-dt.y)).r;
	float n4 = texture2D(u_buffer, vec2( off.x-dt.x, off.y)).r;
	float n5 = texture2D(u_buffer, vec2( off.x-dt.x, off.y+dt.y)).r;
	float n6 = texture2D(u_buffer, vec2( off.x, off.y-dt.y)).r;
	float n7 = texture2D(u_buffer, vec2( off.x, off.y+dt.y)).r;

	float sum = min(n0+n1+n2+n3+n4+n5+n6+n7, 7.0);

	float a = texture2D(u_buffer, st).r;

	sum += 8.0 * step( 0.5, a );

	int ix = int(mod(sum,4.0));
	int iy = int(sum/4.0);

    gl_FragColor = vec4( vec3( RULES[iy][ix]), 1.0 );
}
