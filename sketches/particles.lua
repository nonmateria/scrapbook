
require "strict"

----------------------------------------------------
window.size( 640, 640 )

layer.create( "def")
layer.clear(0, 0, 0, 255 ) 

local vstep = 16
local vw = layer.width()/vstep
local vh = layer.height()/vstep

ps.setup_particles( 8092 )
ps.setup_vectors( vw, vh, vstep ) 

ps.drag( 0.24 ) 
ps.speed( 1.2 ) 
ps.lifespan( 60 ) 

----------------------------------------------------
function loop()
	window.title( "fps:"..window.fps().." | p:"..ps.get_size() ) 

	if rand.chance( 0.8 ) then
		local x = layer.width() * rand.range( -0.05, 0.05 )
		local y = layer.height() * rand.range( -0.05, 0.05 )
		ps.spawn( x, y )
	end

	if rand.chance( 0.8 ) then
		local x = layer.width() * rand.range( 0.95, 1.05 )
		local y = layer.height() * rand.range( 0.95, 1.05 )
		ps.spawn( x, y )
	end

	local x = layer.width() * 0.5
	local y = layer.height() * 0.5
	ps.attract( x, y, 20.0 ) 
	ps.rotate( x, y, 250.0 )
	
	-- updates and apply vector field --
	local t = clock.get() * 0.05
	local twopi = 4.0*math.pi
	local nmult = 1.5 
	for vx=1,vw do 
		for vy=1,vh do
			local xx = nmult * vx / vw
			local yy = nmult * vy / vw
			local angle = fn.noise3( xx, yy, t ) * twopi
			local str = fn.noise3( xx, yy, t+10.0 )
			local ax = math.cos( angle ) * str
			local ay = math.sin( angle ) * str
			ps.set_vector( vx, vy, ax, ay )
		end
	end
	ps.vectors( 10.0 )	 
	
	ps.update()
		
	--layer.clear(0,0,0,255)
	gfx.blend_multiply()
	local fade = 15
	gfx.color( 255 - fade, 255 - fade, 255- fade, 255 )
	gfx.rect_fill( 0, 0, layer.width(), layer.height() ) 

	gfx.blend_additive()

	ps.color( 255, 40, 40, 255 )
	--ps.draw_vectors() -- noise flow debug view 
	ps.draw()
end
