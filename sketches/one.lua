
require "strict"

-- thrustmaster dual analog 4 
da4 = { button_1=7, button_2=8, button_3=9, button_4=5,
		pad_up=1, pad_down=3, pad_left=4, pad_right=2,
		left_axis_x=0, left_axis_y=1, right_axis_x=2, right_axis_y=3,
		button_left=9, button_right=11, button_select=13, button_start=15 }

----------------------------------------------------

local w = 160 
local h = 160
local ds = 2

window.size( w, h )

layer.create("def", w/ds, h/ds, ds)

local cx = 1920 / 2
local cy = 1080 / 2 
local agility = 3.0

----------------------------------------------------
function loop()
	local t = clock.get()

	if pad.pressed( da4.button_1 ) then
		clock.warp( 5.5 )
		cx =  cx + 3.0 * fn.noise1( t*1.72 ) - 1.5
		cy =  cy + 3.0 * fn.noise1( t*1.81 ) - 1.5
		agility = 7.0 
	elseif pad.released( da4.button_1 ) then
		agility = 3.0
		clock.warp( 1.0 )
	end
	cx = cx + pad.axis( da4.left_axis_x ) * agility
	cy = cy + pad.axis( da4.left_axis_y ) * agility

	window.position( cx, cy )

    layer.clear(0,0,0,0)
    gfx.color(255,255,255,255)

	gfx.push()
	    gfx.translate( (w/ds)/2, (h/ds)/2 )

	    gfx.poly( 0, 0, 10, 3, gfx.pi )

	    local off = 20

	    gfx.rotate( fn.ramp(t*0.2) * fn.two_pi )
	    
	    for i=1,3 do 
	        gfx.push()
	        gfx.rotate( fn.triangle( t*(0.17 + i*0.05) ) * fn.two_pi )
	        gfx.circle( off, 0, 2 )
	        gfx.point( -off, 0 )
	        gfx.point( -(off+2), 0 )
	        gfx.point( -(off+4), 0 )
	        gfx.pop()
	    end
    gfx.pop()  
end
