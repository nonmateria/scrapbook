
#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.1415926535897932384626433832795
#define TWO_PI 6.2831853071795864769252867665590

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_buffer;

// ------------------- FUNCTIONS -------------------------------
float rand(vec2 st, float t){
    return fract(sin(dot(st.xy + fract(t*0.0013) ,vec2(12.9898,78.233))) * 43758.5453);
}

// ------------------- SHADER ----------------------------------
void main(){  
    vec2 st = gl_FragCoord.xy/u_resolution;
	float rndy = rand( st, u_time + 12.0 );
	st.y -= 0.001 + rndy*0.009;

	float fb = 0.75;
    vec4 source = texture2D( u_buffer, st ); // for texture access
    vec4 color = source*fb + vec4( vec3(1.0-fb), 1.0 );
    color.a = 1.0;
    gl_FragColor = color;	
}
