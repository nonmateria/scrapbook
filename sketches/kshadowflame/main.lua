
require "strict"

----------------------------------------------------
local kw = 640
local kh = 480
local mult = 1

window.size( kw * mult, kh * mult)

layer.create( "cam", kw, kh, mult)
	layer.filter_linear()
	layer.hide()
layer.create( "fb", kw, kh, mult )
	layer.filter_linear()

cam.open( "/dev/video2", kw, kh )

frag.load( "threshold.frag" ) 
frag.load( "feedback.frag" ) 

----------------------------------------------------
function loop()
	layer.select( "cam")
	layer.clear(0,0,0,255)
	cam.draw(0, 0)

    frag.apply( "threshold" ) 

    layer.select( "fb" )
    frag.apply( "feedback" )
	layer.draw( "cam", 0, 0 ) 
end
