
#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.1415926535897932384626433832795
#define TWO_PI 6.2831853071795864769252867665590

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_buffer;

// ------------------- FUNCTIONS -------------------------------
float rand(vec2 st, float t){
    return fract(sin(dot(st.xy + fract(t*0.0013) ,vec2(12.9898,78.233))) * 43758.5453);
}

// ------------------- SHADER ----------------------------------
void main(){  
    vec2 st = gl_FragCoord.xy/u_resolution;

	vec2 off = vec2(0.0);
	off.x = rand( st, u_time )-0.5;
	off.y = rand( st, u_time + 12.0 )-0.5;
	off *= 0.005;
	st += off;
	
    vec4 source = texture2D( u_buffer, st ); // for texture access

	float c = smoothstep( 0.9, 0.95, source.r );

	float a = 1.0 - step( 0.98, c );
    gl_FragColor = vec4( vec3(c), a ) ;
}
