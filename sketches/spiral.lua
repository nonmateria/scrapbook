
-- from tutorial:
-- https://necessarydisorder.wordpress.com/2019/02/20/distortion-or-smoke-effect-on-parametric-curves/

require "strict"

----------------------------------------------------
window.size( 480, 480 )
layer.create( "def")
layer.clear(0,0,0,255)

----------------------------------------------------
function loop()
	gfx.translate( 240, 240 )

	local fade = 200  
	gfx.color( fade, fade, fade, 255 )
	gfx.blend_multiply()
	gfx.rect_fill( -240, -240, layer.width(), layer.height() ) 

	local points = 5000
	local tstep = 35 / points
	local rstep = 220 / points

	local off = fn.ramp(-clock.get()*0.04 ) * 2*math.pi
	local g = 0.05
	local t = clock.get() * 0.4
	local delta = 0.004

	gfx.blend_alpha()

	for i=0,points do
		local theta = i*tstep + off
		local radius = i*rstep
		local a = fn.map( i, 0, points, 255, 40 )

		for n=1,3 do
			local x = math.cos( theta ) * radius
			local y = math.sin( theta ) * radius
			local seed = n*10
			local nox = fn.noise2( x*g + t, seed + y*g ) - 0.5
			x = x + nox * i * delta 
			local noy = fn.noise2( x*g + t, seed*2 + y*g ) - 0.5
			y = y + noy * i * delta 

			gfx.color( a, a, a, 255 )
			gfx.point( x, y )
		end
	end
end
