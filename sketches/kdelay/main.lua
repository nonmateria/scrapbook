
require "strict"

----------------------------------------------------
local kw = 640
local kh = 480

window.size( kw, kh )
layer.create( "def")

cam.open( "/dev/video2", kw, kh )

local kmax = 8*4
local ki = 1 

print( "allocating delay buffers..." )

frag.load( "threshold.frag" )

for i=1,kmax do
	layer.create( "ring"..i, kw, kw )
	layer.hide()
end

print( "..done" )

----------------------------------------------------
function loop()
	update_ring()

	layer.select( "def" ) 
	layer.clear(0,0,0,255)

	for iv=0,4 do
		local i = 4-iv
		local gray = 255 - i*55
		layer.color( 255, gray, gray, 255 )
		layer.draw( delayed(i*8), 0, 0 )  
	end
end

----------------------------------------------------
function update_ring()
	ki = ki + 1
	if ki > kmax then ki = 1 end 
	layer.select( "ring"..ki ) 
	
	cam.draw(0, 0)

	frag.apply( "threshold" ) 
end

function delayed( d ) 
	local i = ki - d 
	if i <= 0 then i = i + kmax end 
	return "ring"..i
end 
