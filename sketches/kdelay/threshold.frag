
#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.1415926535897932384626433832795
#define TWO_PI 6.2831853071795864769252867665590

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_buffer;

// ------------------- FUNCTIONS -------------------------------


// ------------------- SHADER ----------------------------------
void main(){
  
    vec2 st = gl_FragCoord.xy/u_resolution;

    //float ratio = u_resolution.x / u_resolution.y;
    //st.x *= ratio;

    vec4 source = texture2D( u_buffer, st ); // for texture access

	float a = step ( source.r, 0.85 );
    
    gl_FragColor = vec4( 1.0, 1.0, 1.0, a );

}
