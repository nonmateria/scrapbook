
require "strict"

----------------------------------------------------
window.size( 640, 16*22 )
layer.create( "def")

----------------------------------------------------
function loop()
	layer.clear(10,20,20,255)

	local x = 20
	local size = 16
	local y = 16*21 - size
	gfx.color( 200, 200, 200, 255 )
	gfx.rect_fill( x, y, size, size ) 
	gfx.text( ""..size.."x"..size, x, y-size-12 ) 

	x = x + size + 50
	size = 32
	y = 16*21 - size

	gfx.rect_fill( x, y, size, size ) 
	gfx.text( ""..size.."x"..size, x, y-24 ) 

	x = x + size + 50
	size = 48
	y = 16*21 - size
	gfx.rect_fill( x, y, size, size ) 
	gfx.text( ""..size.."x"..size, x, y-24 ) 

	x = x + size + 40
	size = 64
	y = 16*21 - size
	gfx.rect_fill( x, y, size, size ) 
	gfx.text( ""..size.."x"..size, x, y-24 ) 

	x = x + size + 40
	size = 96
	y = 16*21 - size
	gfx.rect_fill( x, y, size, size ) 
	gfx.text( ""..size.."x"..size, x, y-24 ) 

	x = x + size + 40
	size = 128
	y = 16*21 - size
	gfx.rect_fill( x, y, size, size ) 
	gfx.text( ""..size.."x"..size, x, y-24 ) 

	--[[
	x = x + size + 40
	y = 
	size = 128
	gfx.rect_fill( x, y, size, size ) 
	gfx.text( ""..size.."x"..size, x, y+16+size ) 
--]]

	x = 16
	y = 16
	local w = 16*10
	local h = 16*14
	gfx.rect_fill( x, y, w, h ) 
	gfx.text( ""..w.."x"..h, x, y+h+6 ) 
	
	
	x = 16*14
	y = 16
	local w = 240
	local h = 160
	gfx.rect_fill( x, y, w, h ) 
	gfx.text( ""..w.."x"..h.." (gba)", x, y+h+10 ) 

	
	gfx.text( "640x356", 640-100, 20  ) 
	
	
	if key.pressed( key.s ) then
		layer.save( "dims.png" )
		print "frame saved"
	end
end
