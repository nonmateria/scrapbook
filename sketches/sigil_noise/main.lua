
require "strict"

local w = 480
local h = 800 

----------------------------------------------------
window.size( w, h )

png.load( "/assets/ink/tests/sigil_alpha.png", "sigil" )

frag.load( "aura.frag", "aura" )
frag.load( "dither.frag" )

layer.create( "default", w, h, 1 )
png.align_center()

----------------------------------------------------

local clock = 0
local col = 0

function loop()
    frag.select( "aura" )
        frag.uniform( "u_background", 0.0, 0.0, 0.0 )
        frag.uniform( "u_feedback", 0.99 )
    frag.apply()

   	gfx.push()
        gfx.scale( 2.0, 2.0 )
        
        if clock == 0 then
            if col == 0 then 
                col = 255        
            else
                col = 0
            end
            clock = 160
        end
        clock = clock-1
        
        png.color( col, col, col, 255 )
        png.select( "sigil" )
        png.draw( window.width()/4, window.height()*1.15 / 4 )  
	gfx.pop()

	-- avoid feedback from up to bottom border
	gfx.color( 0, 0, 0, 255 )
	gfx.rect_fill( 0, h*0.9, w, h*0.1 ) 
end
