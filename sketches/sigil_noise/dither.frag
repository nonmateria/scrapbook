
#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.1415926535897932384626433832795
#define TWO_PI 6.2831853071795864769252867665590

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_tex0;

// ------------------- FUNCTIONS -------------------------------
float dither_cr( float x, vec3 thresholds, float angle ){

	float sin_theta = sin(angle);
	float cos_theta = cos(angle);
	mat2 rot_mat = mat2( cos_theta,-sin_theta, sin_theta, cos_theta); 
	vec2 rot = gl_FragCoord.xy * rot_mat;

	float p1 = step(0.75,fract(rot.x*0.25));
	float p0 = p1 * step(0.75, fract(rot.y/4.0))*0.5;

	float s2 = step( thresholds[2], x );
	float s1 = step( thresholds[1], x ) - s2;
	float s0 = step( thresholds[0], x ) - s1 -s2;
	
	return s0*p0 + s1*p1 + s2*1.0;
}


// ------------------- SHADER ----------------------------------
void main(){
    vec2 st = gl_FragCoord.xy/u_resolution;

    vec4 source = texture2D( u_tex0, st ); // for texture access
    float a =  dither_cr( source.r, vec3( 0.15, 0.3, 0.5), TWO_PI*0.099);
    gl_FragColor = vec4( vec3(a), 1.0) ;

    //gl_FragColor = source;
}
