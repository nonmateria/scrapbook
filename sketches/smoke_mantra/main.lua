
require "strict"

local count = 1
local timer = 0

----------------------------------------------------
window.size( 480, 480 )

png.load( "/assets/ink/tests/write_alpha.png",  "phrase" )
png.align_center()

frag.load(  "dissolve.frag", "dissolve" )

layer.create("def")
layer.filter_linear()
layer.clear( 255, 255, 255, 255 )

----------------------------------------------------
function loop()

    frag.select( "dissolve" )
        frag.uniform( "u_evaporate", 1.5 )
    frag.apply()
    
	if timer == 0 then 
		local step = window.height() / 9

		png.color( 0, 0, 0, 255 )
		png.select( "phrase" )
		png.draw( window.width()/2,  step * count )  

		count = count + 1 
		if count == 9 then
			count = 1
		end
		timer = 60
	end
	timer = timer - 1
end
