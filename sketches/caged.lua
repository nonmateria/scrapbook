
-- from tutorial:
-- https://necessarydisorder.wordpress.com/2018/03/31/a-trick-to-get-looping-curves-with-lerp-and-delay/

require "strict"

----------------------------------------------------
window.size( 480, 480 )
layer.create( "def")

----------------------------------------------------
local seed = rand.range(10, 1000)
local t = 0 

function loop()
	layer.clear(0,0,0,255)

	gfx.translate( 240, 240 )

	-- TODO try audio envelope to mod 
	local mod = fn.sine(clock.get()*0.15)
	t = t - 0.007*mod - 0.005
	
	local points = 2000
	local vertices = 4
	local delta = 20
	local delay = 5
	local tstep = 2*math.pi / vertices
	local rad = 230
	local movement = 130

	for n=1,vertices do 
		local theta = n*tstep

		for b=1,2 do
			for i=0,points do 
				local pct = i/points
				local apct = 1.0-pct
				local offset = t - pct*delay * 0.1
				local vx = math.cos(theta+offset) * rad
				local vy = math.sin(theta+offset) * rad

				local cx = (fn.noise2( t - apct*delay, seed*b )  -0.5) * movement
				local cy = (fn.noise2( t - apct*delay, seed*2*b )-0.5) * movement

				local px = vx * pct + cx * apct
				local py = vy * pct + cy * apct

				local a = apct * 205 + 50
				gfx.color( a, a, a, 255 ) 
				gfx.point( px, py ) 
			end
		end
	end

	local cx0 = (fn.noise2( t - delay, seed )  -0.5) * movement
	local cy0 = (fn.noise2( t - delay, seed*2 )-0.5) * movement
	
	local cx1 = (fn.noise2( t - delay, seed*2 )  -0.5) * movement
	local cy1 = (fn.noise2( t - delay, seed*4 )-0.5) * movement

	local theta = math.atan2( cy0-cy1, cx0-cx1 ) - math.pi * 0.5

	gfx.color( 255, 255, 255, 255 ) 
	gfx.circle( cx0, cy0, 5 ) 
	gfx.push()
		gfx.translate( cx0, cy0 )
		gfx.rotate( theta ) 
		gfx.color( 0, 0, 0, 255 ) 
		gfx.poly_fill( 0, 16, 16, 3 ) 
		gfx.color( 255, 255, 255, 255 ) 
		gfx.poly( 0, 16, 16, 3 ) 
	gfx.pop() 
		
	gfx.color( 255, 255, 255, 255 ) 
	gfx.circle( cx1, cy1, 5 ) 
	gfx.push()
		gfx.translate( cx1, cy1 )
		gfx.rotate( theta + math.pi ) 
		gfx.color( 0, 0, 0, 255 ) 
		gfx.poly_fill( 0, 16, 16, 3 ) 
		gfx.color( 255, 255, 255, 255 ) 
		gfx.poly( 0, 16, 16, 3 ) 
	gfx.pop() 

	local offset = t - delay * 0.1
	gfx.poly( 0, 0, rad, 4, offset )
end
