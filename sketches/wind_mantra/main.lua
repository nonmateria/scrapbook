
require "strict"

----------------------------------------------------
local w = 480
local h = 800

window.size( w, h )
window.fade_in( 500 )
window.fade_out( 500 )

png.load( "/assets/ink/tests/write_alpha.png", "phrase" )
png.align_center()

frag.load( "evaporate.frag" )

layer.create( "def" )
layer.filter_linear()
layer.clear(255,255,255,255)

----------------------------------------------------
local clock = 0

function loop()
    frag.select( "evaporate" )
        frag.uniform( "u_background", 1.0, 1.0, 1.0 )
        frag.uniform( "u_evaporate", 0.5 )
        frag.uniform( "u_wind", 0.0004 )
    frag.apply()

	gfx.scale( 2.0, 2.0 )

	if clock == 0 then
		clock = 160

		png.select( "phrase" )		
		png.color(0, 0, 0, 255)    
		png.draw( w*0.25, h*0.33 )  
	end
	clock = clock-1
end
