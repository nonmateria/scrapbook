
require "strict"

----------------------------------------------------
window.size( 480, 800 )
window.title( "--" )

frag.load( "background.frag", "background" )
frag.load( "glitch.frag", "glitch" )

layer.create( "bg" )
layer.clear( 0, 0, 0, 255 )

layer.create("def")

----------------------------------------------------
function loop()

    frag.apply( "background" ) -- also clears the background

	gfx.push()
    gfx.translate( window.width()*0.5, window.height()*0.5 )

    local offset = 0.1 * window.width()
    local side = 0.05 * window.width()
    local stroke = 0.02 * window.width()

	local t = clock.get()
    for i=1,3 do 
        local m0 = fn.triangle( t * (0.174 + i*0.019) )
		
        gfx.color( 255, 255, 255, m0*255 )
		
		gfx.poly( 0.0, offset*i,  side, 4 )
		gfx.poly( 0.0, offset*i,  side-stroke, 4 )
		
		gfx.poly( 0.0, -offset*i, side, 4 )
		gfx.poly( 0.0, -offset*i, side-stroke, 4 )
		
        local m1 = fn.triangle( t * (0.2 + i*0.017) ) 
        
        gfx.color( 255, 255, 255, m1*255 )
		
		gfx.poly( offset*i, 0, side, 4 )
		gfx.poly( offset*i, 0, side-stroke, 4 )

		gfx.poly( -offset*i, 0, side, 4 )
		gfx.poly( -offset*i, 0, side-stroke, 4 )
    end
    gfx.pop()

    frag.apply( "glitch" )
end

----------------------------------------------------
function exit()

end


