
#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.1415926535897932384626433832795
#define TWO_PI 6.2831853071795864769252867665590

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

vec3 u_color_a = vec3( 1.0 );
vec3 u_color_b = vec3( 1.0 );

//
// Description : GLSL 2D simplex noise function
//      Author : Ian McEwan, Ashima Arts
//  Maintainer : ijm
//     Lastmod : 20110822 (ijm)
//     License :
//  Copyright (C) 2011 Ashima Arts. All rights reserved.
//  Distributed under the MIT License. See LICENSE file.
//  https://github.com/ashima/webgl-noise
//

// Some useful functions
vec3 mod289(vec3 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
vec2 mod289(vec2 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
vec3 permute(vec3 x) { return mod289(((x*34.0)+1.0)*x); }

float noise(vec2 v) {

    // Precompute values for skewed triangular grid
    const vec4 C = vec4(0.211324865405187,
                        // (3.0-sqrt(3.0))/6.0
                        0.366025403784439,
                        // 0.5*(sqrt(3.0)-1.0)
                        -0.577350269189626,
                        // -1.0 + 2.0 * C.x
                        0.024390243902439);
                        // 1.0 / 41.0

    // First corner (x0)
    vec2 i  = floor(v + dot(v, C.yy));
    vec2 x0 = v - i + dot(i, C.xx);

    // Other two corners (x1, x2)
    vec2 i1 = vec2(0.0);
    i1 = (x0.x > x0.y)? vec2(1.0, 0.0):vec2(0.0, 1.0);
    vec2 x1 = x0.xy + C.xx - i1;
    vec2 x2 = x0.xy + C.zz;

    // Do some permutations to avoid
    // truncation effects in permutation
    i = mod289(i);
    vec3 p = permute(
            permute( i.y + vec3(0.0, i1.y, 1.0))
                + i.x + vec3(0.0, i1.x, 1.0 ));

    vec3 m = max(0.5 - vec3(
                        dot(x0,x0),
                        dot(x1,x1),
                        dot(x2,x2)
                        ), 0.0);

    m = m*m ;
    m = m*m ;

    // Gradients:
    //  41 pts uniformly over a line, mapped onto a diamond
    //  The ring size 17*17 = 289 is close to a multiple
    //      of 41 (41*7 = 287)

    vec3 x = 2.0 * fract(p * C.www) - 1.0;
    vec3 h = abs(x) - 0.5;
    vec3 ox = floor(x + 0.5);
    vec3 a0 = x - ox;

    // Normalise gradients implicitly by scaling m
    // Approximation of: m *= inversesqrt(a0*a0 + h*h);
    m *= 1.79284291400159 - 0.85373472095314 * (a0*a0+h*h);

    // Compute final noise value at P
    vec3 g = vec3(0.0);
    g.x  = a0.x  * x0.x  + h.x  * x0.y;
    g.yz = a0.yz * vec2(x1.x,x2.x) + h.yz * vec2(x1.y,x2.y);
    return 130.0 * dot(m, g);
}


// if this random fails check out
// http://byteblacksmith.com/improvements-to-the-canonical-one-liner-glsl-rand-for-opengl-es-2-0/
float rand(vec2 st, float t){
    return fract(sin(dot(st.xy + fract(t*0.0013) ,vec2(12.9898,78.233))) * 43758.5453);
}

float rand( in float i, in float t ){
    return fract(sin(dot( vec2(i, t), vec2(12.9898,78.233))) * 43758.5453);
}

float stroke( float x, float d, float w, float fade ){ 
    return smoothstep(d-fade, d+fade, x+w*.5) - smoothstep(d-fade, d+fade, x-w*.5); 
} 
    
float stroke(float x, float d, float w){ 
    float r = step(d,x+w*.5) - step(d,x-w*.5); 
    return clamp(r, 0., 1.); 
}

mat2 rotate2d( in float _angle ){ 
    return mat2(cos(_angle),-sin(_angle), sin(_angle),cos(_angle)); 
}

vec2 rotated( vec2 _st, in float _angle ){ 
    _st -= 0.5; 
    _st *= rotate2d( _angle ); 
    _st += 0.5; 
    return _st;
}


float map( float value, float minin, float maxin, float minout, float maxout ){ 
    value -= minin; 
    value = max( value, 0.0 ); 
    float range = maxin - minin; 
    value = min( value, range ); 
    float pct = value / range; 
    return mix( minout, maxout, pct );
}


// ------------------------------------------------------------------------------

float rangrid( vec2 st, float cols, float rows ){
    vec2 grid = floor( vec2(st.x * cols, st.y * rows) );
    float r = rand( grid, u_time );
    r = floor( r*4.99999 ) * 0.25;
    return r;
}

float rectjit( vec2 st ){

    float xb = 2.0;
    float xb_jitter = 10.0;
    float yb = 24.0;
    float yb_jitter = 10.0;
    float density = 0.2;

    // mult grids    
    float t0 = rangrid( st, xb + rand(0., u_time)*xb_jitter, yb + rand(1., u_time) * yb_jitter );
    t0 = step( t0, density );
    float t1 = rangrid( st, xb + rand(2., u_time)*xb_jitter, yb + rand(3., u_time) * yb_jitter );
    t1 = step( t1, density);
    
    return t0 * t1;
}

void main(){
    vec2 st = gl_FragCoord.xy/u_resolution.xy;
    
    st.x *= u_resolution.x / u_resolution.y;

    vec2 dt = rotated( st, PI * 0.05 );

    float t;

    // noise bands
    float t0 = noise( vec2( dt.y*4.0, u_time ) );
    t0 = step( 0.75, t0 );
    
    // classic static
    float dust = rand( st, u_time ); 
    dust = map( dust, 0.9, 1.0, 0.0, 1. );
    
    // scanlines y
    float t10 = step( 0.5, fract( st.y * u_resolution.y*0.25) );
    
    float t11 = rectjit( dt );

    t = t11*t0 + dust;

    t = min( t, 1.0 );
    
    float a = t * 0.4;
    
    gl_FragColor = vec4( mix( vec3(0.0), u_color_b, a), t );  
    
}
