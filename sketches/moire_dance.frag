
#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.1415926535897932384626433832795
#define TWO_PI 6.2831853071795864769252867665590

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_tex0;

// ------------------- SHADER ----------------------------------
void main(){
    vec2 st = gl_FragCoord.xy/u_resolution;
    
    float ratio = u_resolution.x / u_resolution.y; 
    st.x *= ratio;

    float d = 0.2;
    d *= cos(st.y*u_resolution.y*0.8 + u_time);
    d *= fract( st.y * u_resolution.y * 0.12);
    // d *= fract( st.y * u_resolution.y * 0.133333); // flower

	st.x += d;
	st.x += cos(st.y*4.0 + u_time*0.1)*0.05;

	const float w = 0.05;
	float l = step( st.x, 0.5+w );	
    float r = step(0.5-w, st.x); 
	float a = l*r;

    gl_FragColor = vec4( vec3(1.0), a );
}
