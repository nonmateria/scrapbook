
require "strict"

----------------------------------------------------

window.size( 480, 480 )
layer.create("def")

----------------------------------------------------
function loop()  
    layer.clear( 0, 0, 0, 255 )
   
    local cx = window.width() * 0.5
    local cy = window.height() * 0.5
    local wi = 100
    local he = 100 
    local spacing = 5

	gfx.push()
		gfx.color( 255, 255, 255, 255 )
	    distolines( cx, cy, cx-wi, cy-he )
	    gfx.translate( spacing, 0 )
	    distolines( cx, cy, cx+wi, cy-he )
	    gfx.translate( 0, spacing )
	    distolines( cx, cy, cx+wi, cy+he )
	    gfx.translate( -spacing, 0 )
	    distolines( cx, cy, cx-wi, cy+he )
	gfx.pop()
    
    if key.pressed( key.space ) then
		window.quit()
    end
end

----------------------------------------------------
function distolines( x0, y0, x1, y1 )
    local dx = x1 - x0 
    local dy = y1 - y0
    
    gfx.push()
        gfx.translate( x0, y0 )
        
        for i=1, 10 do
            local fx = rand.range( 0, dx )
            local fy = rand.range( 0, dy )
            
            if i%2 == 0 then
                gfx.line( 0, 0, fx, 0 )
                gfx.line( fx, 0, fx, fy )            
            else
                gfx.line( 0, 0, 0, fy )
                gfx.line( 0, fy, fx, fy )     
            end
        end 
    gfx.pop()
end
