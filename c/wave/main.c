
/*
 * minimal boilerplate to write audio to an output.wav file
 *
 * you need only libsndfile 
 * for example on debian: 
 * 		sudo apt-get libsndfile1 libsndfile1-dev
 *
 * with the included build script you can do
 * 		./build_and_run.sh
 * from the project folder to build it and execute it right away
 */

#include <stdio.h>
#include <stdlib.h>

#include <sndfile.h>

int main(void)
{
	// --------- creating buffer ---------------
	float *buffer = NULL;
	size_t buffersize = 44100;

	buffer = malloc(sizeof(float) * buffersize);
	if (buffer == NULL) {
		printf("error allocating buffer memory\n");
		goto error;
	}

	// fill the buffer with random
	for (unsigned n = 0; n < buffersize; ++n) {
		float r = (float)rand();
		r /= (float)RAND_MAX;
		r = r * 2.0f - 1.0f;
		buffer[n] = r * 0.5f;
	}

	// --------- writing file ------------------
	SF_INFO sfinfo;
	sfinfo.channels = 1;
	sfinfo.samplerate = 44100;
	sfinfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_24;

	const char *path = "output.wav";

	SNDFILE *outfile = sf_open(path, SFM_WRITE, &sfinfo);
	sf_count_t count = sf_write_float(outfile, &buffer[0], (sf_count_t)buffersize);
	sf_write_sync(outfile);
	sf_close(outfile);

	(void)count;

	free(buffer);

	printf("done writing wave\n");

	return 0;

error:
	return 1;
}
