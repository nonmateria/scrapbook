#!/bin/sh

CFLAGS="-std=c99 -pipe -finput-charset=UTF-8 -g -Wall -Wpedantic -Wextra -Wwrite-strings -Wconversion -Wshadow -Wstrict-prototypes -Werror=implicit-function-declaration -Werror=implicit-int -Werror=incompatible-pointer-types -Werror=int-conversion -Os -DNDEBUG"

LDFLAGS="-lsndfile"

cc $CFLAGS $LDFLAGS main.c -o wave
./wave

exit