
/*
 * minimal boilerplate to write audio to an output.wav file
 *
 * you need only libsndfile 
 * for example on debian: 
 * 		sudo apt-get libsndfile1 libsndfile1-dev
 *
 * with the included build script you can do
 * 		./build_and_run.sh
 * from the project folder to build it and execute it right away
 */

#include <stdio.h>
#include <stdlib.h>

#include <math.h>
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

#include <sndfile.h>


int main(void)
{
	// --------- creating buffer ---------------
	float *buffer = NULL;
	size_t buffersize = 2048;

	buffer = malloc(sizeof(float) * buffersize);
	if (buffer == NULL) {
		printf("error allocating buffer memory\n");
		goto error;
	}

	const int samples = buffersize / 2;
	int offset = ((int)buffersize - samples ) / 2;

	for( int i=0; i<offset; ++i ){
		buffer[i] = 0.0f;
	}

	// generate a sine wave windowed by a gaussian function	

	const double gslope = 0.3;

	for (int n = 0; n < samples; ++n) {
		float phase = (double) n / (double) samples;
		float s = (float) -cos( 2.0 * M_PI * 5.0 * phase );

		double Nslash2 = 0.5 * (double) samples;
		
		double p2 = ((double)n - Nslash2) / (gslope*Nslash2);
		p2 *= p2;
		float g = exp( -0.5 * p2 ); 		
		buffer[n+offset] = g * s;
	}

	int start = offset + (int)buffersize;
	for( int i=start; i<(int)buffersize; ++i ){
		buffer[i] = 0.0f;
	}

	// --------- writing file ------------------
	SF_INFO sfinfo;
	sfinfo.channels = 1;
	sfinfo.samplerate = 44100;
	sfinfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_24;

	const char *path = "morlet.wav";

	SNDFILE *outfile = sf_open(path, SFM_WRITE, &sfinfo);
	sf_count_t count = sf_write_float(outfile, &buffer[0], (sf_count_t)buffersize);
	sf_write_sync(outfile);
	sf_close(outfile);

	(void)count;

	free(buffer);

	printf("done writing wave\n");

	return 0;

error:
	return 1;
}
