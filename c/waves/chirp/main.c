
/*
 * minimal boilerplate to write audio to an output.wav file
 *
 * you need only libsndfile 
 * for example on debian: 
 * 		sudo apt-get libsndfile1 libsndfile1-dev
 *
 * with the included build script you can do
 * 		./build_and_run.sh
 * from the project folder to build it and execute it right away
 */

#include <stdio.h>
#include <stdlib.h>

#include <math.h>
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

#include <sndfile.h>


int main(void)
{
	// --------- creating buffer ---------------
	float *buffer = NULL;

	const int ITERATIONS = 3;
	const int BASELEN = 1024;

	int buffersize = 0;
	int len = BASELEN;
	for( int i=0; i<ITERATIONS; ++i ){
		buffersize += len;
		len = len / 2;
	}

	buffer = malloc(sizeof(float) * (size_t)buffersize);
	if (buffer == NULL) {
		printf("error allocating buffer memory\n");
		goto error;
	}

	const int samples = buffersize;
	int offset = ((int)buffersize - samples ) / 2;

	for( int i=0; i<offset; ++i ){
		buffer[i] = 0.0f;
	}

	// generate an exponential chirp waveform 
	len = BASELEN;

	int start = 0;
	for( int i=0; i<ITERATIONS; ++i ){
		for (int n = start; n < start+len; ++n) {
			float phase = (float) (n-start) / (float) (len);
			float s = (float) sin( 2.0 * M_PI * phase );
			buffer[n] = s;
		}
		start += len;
		len = len / 2;
	}

	// --------- writing file ------------------
	SF_INFO sfinfo;
	sfinfo.channels = 1;
	sfinfo.samplerate = 44100;
	sfinfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_24;

	const char *path = "chirp.wav";

	SNDFILE *outfile = sf_open(path, SFM_WRITE, &sfinfo);
	sf_count_t count = sf_write_float(outfile, &buffer[0], (sf_count_t)buffersize);
	sf_write_sync(outfile);
	sf_close(outfile);

	(void)count;

	free(buffer);

	printf("done writing wave\n");

	return 0;

error:
	return 1;
}
