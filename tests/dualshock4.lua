
require "strict"

-- sony dual shock 4
ds4 = { x=7, s=8, o=6, t=5,
		up=1, down=3, left=4, right=2,
		l1=9, l2=10, r1=11, r2=12, share=13, options=15, 
		left_axis_x=0, left_axis_y=1, right_axis_x=2, right_axis_y=3,
		left_trigger=4, right_trigger=5,
		left_axis=16, right_axis=17 }

----------------------------------------------------

window.size( 480, 220 )
layer.create( "def")
--window.background( 20, 20, 20, 255 )

local test = args.get_bool( "-t" )

----------------------------------------------------
function loop()
	layer.clear(0,0,0,255)

	local la = pad.axis( ds4.left_trigger ) 
	gfx.rect_fill( 10, 10, 20, 100 + la * 100 ) 
	gfx.rect( 10, 10, 20, 200 ) 

	for i=0,1 do 
		local ax = 0 + i*2
		local ay = 1 + i*2
		local rx = 40 + i*200
		local ry = 10
		local cx = 100 + rx + pad.axis( ax ) * 100
		local cy = 100 + ry + pad.axis( ay ) * 100
		 
		gfx.rect( rx, ry, 200, 200 ) 
		gfx.circle_fill( cx, cy, 4 ) 
	end 

	local ra = pad.axis( ds4.right_trigger ) 
	gfx.rect_fill( 450, 10, 20, 100 + ra * 100 ) 
	gfx.rect( 450, 10, 20, 200 ) 

	if pad.pressed( ds4.up ) then 
		print( "pad up" )
	end 
	if pad.pressed( ds4.down ) then 
		print( "pad down" )
	end 
	if pad.pressed( ds4.left ) then 
		print( "pad left" )
	end 
	if pad.pressed( ds4.right ) then 
		print( "pad right" )
	end 
	
	if pad.pressed( ds4.x ) then 
		print( "button x" )
	end 	
	if pad.pressed( ds4.o ) then 
		print( "button o (circle)" )
	end 	
	if pad.pressed( ds4.s ) then 
		print( "button s (square)" )
	end 	
	if pad.pressed( ds4.t ) then 
		print( "button t (triangle)" )
	end 

	if pad.pressed( ds4.l1 ) then 
		print( "button L1" )
	end 	
	if pad.pressed( ds4.l2 ) then 
		print( "button L2 pressed" )
	end 	
	if pad.released( ds4.l2 ) then 
		print( "button L2 released" )
	end 
	if pad.pressed( ds4.r1 ) then 
		print( "button R1" )
	end 	
	if pad.pressed( ds4.r2 ) then 
		print( "button R2 pressed" )
	end 	
	if pad.released( ds4.r2 ) then 
		print( "button R2 released" )
	end 
	
	if pad.pressed( ds4.share ) then 
		print( "share button" )
	end 
	if pad.pressed( ds4.options ) then 
		print( "options button" )
	end	
	if pad.pressed( ds4.left_axis ) then 
		print( "left axis pressed" )
	end 	
	if pad.pressed( ds4.right_axis ) then 
		print( "right axis pressed" )
	end 
end
